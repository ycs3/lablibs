import cv2
import numpy
import math
import random

#
# still useful, but legacy blob tracking functions
#

#from blobseg import find_features
def find_features(im, max_corners, contours=None, mask=None):
    # find features using goodFeaturesToTrack (inside contour/mask if given)
    # im: grayscale image
    cols, rows = im.shape[:2]
    if contours != None and mask == None:
        # if contours exists, create mask using contours
        mask = numpy.zeros((cols, rows), dtype=numpy.uint8)
        cv2.drawContours(mask, contours, -1, 255, -1)
    if mask != None:
        feat = cv2.goodFeaturesToTrack(im, max_corners, .01, 5., mask=mask)
    else:
        feat = cv2.goodFeaturesToTrack(im, max_corners, .01, 5.)

    if contours != None:
        cv2.drawContours(im, contours, -1, 255, 2)
    return feat

#from compatcv import convContoursO
def _convContoursO(contours):
    '''
    convert SimpleCV (list of) contours to OpenCV contours
    '''
    ret = []
    for contour in contours:
        r = [list([list(c)]) for c in contour]
        ret.extend([numpy.array(r, dtype=numpy.int32)])
    return ret

#
# Current blob tracking overview:
# - given a pair of blob sets over two frames
# - find features for each image in grayscale
#   (currently using goodFeaturesToTrack)
# - match features using optical flow
#   (currently using calcOpticalFlowPyrLK)
# - track blobs by looking at blob pairs with most matched features
#
# Future Work:
# - compare color histograms as 'features'
# - use other optical flow methods
# - use information from blobs tracking history
#

def calcBlobFlow(c_g, n_g, c_feat, blob_filter=True, sd_mult=1.):
    '''
    c_g grayscale image of current frame
    n_g grayscale image of next frame
    c_feat features to track from current frame
    blob_filter filter out flow elements [sd_mult]SD from
                mean direction, distance
    returns (new) set of features and matching flow
    '''
    num_feat = len(c_feat)

    c_flow, _, _ = cv2.calcOpticalFlowPyrLK(c_g, n_g, c_feat, None,\
                                            winSize=(15, 15), maxLevel=5)
    
    if blob_filter == True:
        deg, dst = [], []
        deg_m, deg_s, dst_m, dst_s = 0., 0., 0., 0.
        for i in range(num_feat):
            x1, y1 = c_feat[i][0]
            x2, y2 = c_flow[i][0]
            xd, yd = x2 - x1, y1 - y2
            d1 = math.atan2(yd, xd)*180/math.pi
            d2 = numpy.linalg.norm(numpy.array((xd, yd)))
            deg.append(d1)
            dst.append(d2)
        deg_m, deg_s = numpy.mean(deg), numpy.std(deg)
        dst_m, dst_s = numpy.mean(dst), numpy.std(dst)
        goodFlow = []
        for i in range(num_feat):
            d1, d2 = deg[i], dst[i]
            if deg_m-deg_s*sd_mult < d1 and d1 < deg_m+deg_s*sd_mult and\
                    dst_m-dst_s*sd_mult < d2 and d2 < dst_m+dst_s*sd_mult:
                goodFlow.append(i)
        c_feat = c_feat[goodFlow]
        c_flow = c_flow[goodFlow]

    return c_feat, c_flow

class BlobSet:
    def __init__(self, frame_id, im, contours):
        # Create a set of blobs (i.e., image frame)
        # from a list of SimpleCV contours
        # not storing the grayscale image is deliberate (cPickle)
        self.frame_id = frame_id
        self.blob_idx = 0
        self.prev_, self.next_ = None, None
        im = numpy.array(im)
        self.ht, self.wt = im.shape[:2]
        gray = cv2.cvtColor(im, cv2.COLOR_BGR2GRAY)
        self.blobs = []
        for contour in contours:
            b = Blob(self, self.blob_idx, contour, gray)
            self.blob_idx += 1
            self.blobs.append(b)

    def get_image(self, img):
        out_B = numpy.array(img[:,:,0]/10)
        out_G = numpy.array(img[:,:,1]/10)
        out_R = numpy.array(img[:,:,2]/10)
        for blob in self.blobs:
            blob._draw(out_B, out_G, out_R)
        img = numpy.dstack((out_B, out_G, out_R))
        return img

    def get_mask(self):
        ret = numpy.zeros((self.ht, self.wt), dtype=numpy.uint16)
        for blob in self.blobs:
            blob._get_mask(ret)
        return ret

    def trackFrame(self, curr_img, next_img, next_set):
        curr_gray = cv2.cvtColor(curr_img, cv2.COLOR_BGR2GRAY)
        next_gray = cv2.cvtColor(next_img, cv2.COLOR_BGR2GRAY)

        self.next_ = next_set
        next_set.prev_ = self
        for b in self.blobs:
            b.trackBlob(curr_gray, next_gray, next_set)
        for b in next_set.blobs:
            b.reverseTrackBlob(next_gray, curr_gray, self)

blob_id = 0

class Blob:
    def __init__(self, frame, idx, contour, gray, color=None):
        # SimpleCV contour
        global blob_id
        self.blob_id = blob_id
        blob_id += 1

        self.idx = idx
        self.next_, self.prev_ = None, None    # forward matches
        self.rev_prev_, self.rev_next_ = None, None # reverse matches
        self.parent_ = None # parent_ is only valid if less than blob_id
        self.origin_ = self.blob_id
        self.frame = frame
        self.contour, self.contoursO = contour, _convContoursO([contour])
        self.feat = find_features(gray, 500, self.contoursO)
        self.flow_src, self.flow_dst = None, None
        self.rev_flow_src, self.rev_flow_dst = None, None
        if color:
            self.color = color
        else:
            self.color = (random.randint(0, 192),\
                          random.randint(0, 192),\
                          random.randint(0, 192))

    def contains(self, pt):
        ret = cv2.pointPolygonTest(self.contoursO[0], (pt[0], pt[1]), False)
        if ret < 0:
            return False
        return True

    def _track(self, flow_src, flow_dst, comp_frame):
        if len(flow_src) == 0: # no valid source feature
            return None, None, None
        flow_src, flow_dst =\
                zip(*flow_src)[0], zip(*flow_dst)[0]

        # look at dest of flow, see if any go in the blob in comp_frame
        nextBlobTally = [0 for j in range(comp_frame.blob_idx)]
        for pt in flow_dst:
            for b in comp_frame.blobs:
                if b.contains(pt):
                    nextBlobTally[b.idx] += 1
        if len(nextBlobTally) == 0: # no valid destination feature
            return None, None, None
        mx = max(nextBlobTally)
        if mx == 0:
            return None, None, None
        #print ">>>", self.blob_id, nextBlobTally

        # track on next frame is next_idx
        next_idx = nextBlobTally.index(mx)

        return next_idx, flow_src, flow_dst

    def trackBlob(self, curr_gray, next_gray, next_frame):
        if self.feat == None:
            return
        flow_src, flow_dst = calcBlobFlow(curr_gray, next_gray, self.feat)
        #print "TRACK BLOB"
        next_idx, self.flow_src, self.flow_dst =\
                            self._track(flow_src, flow_dst, next_frame)
        if next_idx == None:
            return
        # already assigned a lower blob number
        if next_frame.blobs[next_idx].blob_id < self.blob_id:
            return
        self.next_ = next_frame.blobs[next_idx]
        self.next_.prev_ = self
        self.next_.blob_id = self.blob_id
        self.next_.color = self.color
        self.next_.parent_ = self.parent_
        if self.origin_ < self.next_.origin_:
            self.next_.origin_ = self.origin_
        #if self.parent_:
        #    print "Setting next_parent to be parent", self.parent_.blob_id

    def reverseTrackBlob(self, curr_gray, prev_gray, prev_frame):
        if self.feat == None:
            return
        flow_src, flow_dst = calcBlobFlow(curr_gray, prev_gray, self.feat)
        #print "REV TRACK BLOB"
        prev_idx, self.rev_flow_src, self.rev_flow_dst =\
                            self._track(flow_src, flow_dst, prev_frame)
        if prev_idx == None:
            return
        self.rev_prev_ = prev_frame.blobs[prev_idx]
        self.rev_prev_.rev_next_ = self
        if self.rev_prev_.next_:
            if self.rev_prev_.next_.blob_id != self.blob_id:
                self.parent_ = self.rev_prev_.next_
                if self.rev_prev_.next_.origin_ < self.origin_:
                    self.origin_ = self.rev_prev_.next_.origin_
        else:
            # if a previous forward match did not exist,
            # reverse match should be a correct match
            self.rev_prev_.next_ = self
            self.prev_ = self.rev_prev_
            self.blob_id = self.rev_prev_.blob_id
            self.color = self.rev_prev_.color
            self.parent_ = self.rev_prev_.parent_
            if self.rev_prev_.origin_ < self.origin_:
                self.origin_ = self.rev_prev_.origin_

    def _get_mask(self, mask):
        idx = self.blob_id
        if self.parent_ and self.parent_.blob_id < self.blob_id:
            idx = self.parent_.blob_id
        cv2.drawContours(mask, self.contoursO, -1, idx+1, -1)

    def _draw(self, out_B, out_G, out_R):
        cv2.drawContours(out_B, self.contoursO, -1, self.color[2], -1)
        cv2.drawContours(out_G, self.contoursO, -1, self.color[1], -1)
        cv2.drawContours(out_R, self.contoursO, -1, self.color[0], -1)
        M = cv2.moments(self.contoursO[0])
        cx = int(M['m10']/M['m00'])
        cy = int(M['m01']/M['m00'])
        cv2.putText(out_R, str(self.blob_id), (cx, cy),\
                    cv2.FONT_HERSHEY_COMPLEX, .5, 255)
        cv2.putText(out_G, str(self.blob_id), (cx, cy),\
                    cv2.FONT_HERSHEY_COMPLEX, .5, 255)
        cv2.putText(out_B, str(self.blob_id), (cx, cy),\
                    cv2.FONT_HERSHEY_COMPLEX, .5, 255)
        #if self.parent_ != None and self.parent_.blob_id < self.blob_id:
        #    cv2.putText(out_R, str(self.parent_.blob_id), (cx, cy-10),\
        #                cv2.FONT_HERSHEY_COMPLEX, .5, 255)
        #    cv2.putText(out_B, str(self.parent_.blob_id), (cx, cy-10),\
        #                cv2.FONT_HERSHEY_COMPLEX, .5, 255)
        cv2.putText(out_R, str(self.origin_), (cx, cy-10),\
                    cv2.FONT_HERSHEY_COMPLEX, .5, 255)
        cv2.putText(out_B, str(self.origin_), (cx, cy-10),\
                    cv2.FONT_HERSHEY_COMPLEX, .5, 255)
        if self.prev_ and self.prev_.flow_src:
            for p in range(len(self.prev_.flow_src)):
                sx, sy = [int(i) for i in self.prev_.flow_src[p]]
                dx, dy = [int(i) for i in self.prev_.flow_dst[p]]
                cv2.line(out_G, (sx, sy), (dx, dy), 255, 2)
        if self.rev_flow_src:
            for p in range(len(self.rev_flow_src)):
                sx, sy = [int(i) for i in self.rev_flow_src[p]]
                dx, dy = [int(i) for i in self.rev_flow_dst[p]]
                cv2.line(out_B, (sx, sy), (dx, dy), 255, 3)
