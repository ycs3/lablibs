# -*- coding: utf-8 -*-

#
# camera.py
# Reading source images
#

import cv2
import glob
import numpy
import atexit
import os
#from laceutils import color_depthmap

def _color_depthmap(depth):
    numpy.clip(depth, 0, 2**10 - 1, depth)
    depth >>= 2
    depth = depth.astype(numpy.uint8)
    return depth

def get_file_prefix(path):
    filename, ext = os.path.splitext(os.path.basename(path))
    prefix = filename.rsplit("-",1)[0]
    return prefix, ext[1:]

class Video(object):
    def __init__(self, path):
        self.color_video = path
        self.color_vc = cv2.VideoCapture()
        self.color_vc.open(self.color_video)

        # all frames start at 1
        self.color_frame = 0
        self.frame_no = self.color_frame

        self.color = None

    def gotoFrame(self, frame_no):
        self.color_vc.set(cv2.cv.CV_CAP_PROP_POS_FRAMES, frame_no)
        self.color_frame = frame_no - 1
        self.frame_no = self.color_frame
        return True

    def getFrame(self, read=True):
        ret, color_ = self.color_vc.read()
        if read:
            self.color = color_
        if ret == False:
            return False
        self.color_frame += 1
        self.frame_no = self.color_frame
        return True

class Stream(object):
    def __init__(self, path, color_file="color.avi"):
        self.color_path = path + "/color/"
        self.depth_path = path + "/depth/"
        self.color_jpeg_path = path + "/color_jpeg/"
        self.legacy = False
        try:
            self.color_ts = self.read_timestamps(self.color_path + "timestamps")
        except IOError:
            self.legacy = True
        #self.color_video = self.color_path + color_file
        #self.depth_ts_rev = self.read_timestamps(self.depth_path + "timestamps", reverse=True)
        #self.depth_ts_arr = sorted(self.depth_ts_rev.keys())

        if self.legacy:
            self.file_list = glob.glob(path + "/*-rgb.*")
            self.file_list = sorted(self.file_list)
        else: 
            self.color_video = self.color_path + color_file
            self.depth_ts_rev = self.read_timestamps(self.depth_path + "timestamps", reverse=True)
            self.depth_ts_arr = sorted(self.depth_ts_rev.keys())
            self.color_vc = cv2.VideoCapture()
            self.color_vc.open(self.color_video)

        # all frames start at 1
        self.color_frame = 0
        self.frame_no = self.color_frame
        self.depth_frame = 1

        self.color = None
        self.depth = None

    def read_timestamps(self, filename, reverse=False):
        timestamp = {}
        for line in open(filename).readlines():
            fno, msec = [int(k) for k in line.strip().split()]
            if reverse:
                timestamp[msec] = fno
            else:
                timestamp[fno] = msec
        return timestamp

    def find_closest_frame(self, target, ts_list):
        curr_best = None
        for v in ts_list:
            d = abs(target-v)
            if curr_best == None:
                curr_best = (d, v)
                continue
            if curr_best[0] < d:
                return curr_best[1]
            curr_best = (d, v)
        return -1

    def find_closest_frame_idx(self, target, ts_list, ts_start):
        if ts_start < 1:
            ts_start = 1
        curr_best = None
        for i in range(ts_start-1, len(ts_list)):
            v = ts_list[i]
            d = abs(target-v)
            if curr_best == None:
                curr_best = (d, v, i)
                continue
            if curr_best[0] < d:
                return curr_best[1], curr_best[2]
            curr_best = (d, v, i)
        return -1, -1

    def get_depth_frame(self, dts):
        depth_frame_no = self.depth_ts_rev[dts]
        return cv2.imread(self.depth_path + "%05d.png" % depth_frame_no, 2)

    def gotoFrameLegacy(self, frame_no):
        # frame_no is the value in the filename
        # this may be different from the color_frame/depth_frame numbers
        # (which is used for non-legay functions)
        for color_file_idx in range(len(self.file_list)):
            color_file = self.file_list[color_file_idx]
            color_frame_no = int(color_file.rsplit("-", 3)[-3])
            if color_frame_no == frame_no:
                self.color_frame = color_file_idx
                self.frame_no = self.color_frame
                return True
        return False

    def gotoFrame(self, frame_no):
        if self.legacy:
            return self.gotoFrameLegacy(frame_no)
        self.color_vc.set(cv2.cv.CV_CAP_PROP_POS_FRAMES, frame_no)
        self.color_frame = frame_no - 1
        self.frame_no = self.color_frame
        self.depth_frame = 1
        return True

    def getFrameLegacy(self, read=True):
        try:
            color_file = self.file_list[self.color_frame]
        except IndexError:
            return False
        color_ext = color_file.rsplit(".", 1)[-1]
        color_frame_no = int(color_file.rsplit("-", 3)[-3])
        color_ts = int(color_file.rsplit("-", 3)[-2])
        color_prefix = color_file.rsplit("-", 1)[0]

        try:
            if read:
                self.color = cv2.imread(color_file)
        except:
            return False
        self.color_frame += 1
        self.frame_no = self.color_frame

        depth_file = color_prefix + "-depth." + color_ext
        try:
            if read:
                self.depth = cv2.imread(depth_file, 2)
        except:
            return False
        self.depth_frame = self.color_frame
        #print color_frame_no, color_ts, color_prefix
        return True

    def getFrame(self, read=True):
        if self.legacy:
            return self.getFrameLegacy(read)

        ret, color_ = self.color_vc.read()
        if read:
            self.color = color_
        if ret == False:
            color_ = cv2.imread(self.color_jpeg_path + "%08d.jpg" % (self.color_frame+1))
            if color_ == None:
                return False
            self.color = color_
        self.color_frame += 1
        self.frame_no = self.color_frame

        try:
            cts = self.color_ts[self.color_frame]
        except KeyError:
            return False
        dts, self.depth_frame = self.find_closest_frame_idx(cts, self.depth_ts_arr, self.depth_frame)
        if dts == -1:
            return False
        #self.depth_frame = self.depth_ts_rev[dts]
        #print cts, dts, self.depth_frame, self.depth_ts_rev[dts]
        if read:
            self.depth = self.get_depth_frame(dts)
            # depth may be None
        return True

class KinectImageStream(object):
    '''
    Read a 8-bit color image, 16-bit depth image set

    self.path = (directory of image set)
    self.prefix = (filename prefix)
    self.idx = (frame_idx)
    self.color = (8-bit color image)
    self.depth = (16-bit depth image)
    '''
    def __init__(self, path):
        self.path = path
        self.prefix = None
        self.idx = -1
        self.color = None
        self.depth = None

        self.file_list = glob.glob(path + "/*-rgb.*")
        self.file_list = sorted(self.file_list)
        self._list_max = len(self.file_list)

    def getFrame(self, step=1, read=True):
        # read = False: don't actually read the files
        self.idx += step
        if self.idx >= self._list_max or read == False:
            return None, None
        self.prefix, ext = get_file_prefix(self.file_list[self.idx])
        self.color = cv2.imread(self.path+"/"+self.prefix+"-rgb."+ext)
        self.depth = cv2.imread(self.path+"/"+self.prefix+"-depth."+ext, 2)
        return self.color, self.depth

#
# Example Code
#

def example_KinectImageStream(path):
    c = KinectImageStream(path)
    c.getFrame(100, False)
    try:
        while True:
            f, d = c.getFrame(1)
            if f == None:
                break
            cv2.imshow('RGB', f)
            cv2.imshow('DEPTH', _color_depthmap(d))
            print "IDX", c.idx, "DEPTH.DTYPE", d.dtype
            k = cv2.waitKey(1)
            if k == 32: break
    except KeyboardInterrupt:
        pass

if __name__ == '__main__':
    #istc = Stream("/home/user/devel/wetlab/istc/bioturk_3010-8372/protocol/ds40")
    #istc = Stream("/mnt/other/wetlab/ypad11")
    istc = Stream("/mnt/dvd/uw/bioturk_3010-8279/protocol/ds40")
    istc.gotoFrame(42230)
    index = 0
    try:
        while istc.getFrame():
            cv2.imshow('C', istc.color)
            if istc.depth != None:
                cv2.imshow('D', istc.depth)
            cv2.waitKey(10)
            index += 1
            #if index == 100:
            #    istc.gotoFrame(1)
    except KeyboardInterrupt:
        pass
