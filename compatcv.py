import cv2
import numpy

have_simplecv = False
try:
    from SimpleCV import Image
    have_simplecv = True
except ImportError:
    pass

#
# Compatibility functions between SimpleCV and OpenCV
#

def convContoursO(contours):
    '''
    convert SimpleCV (list of) contours to OpenCV contours
    '''
    ret = []
    for contour in contours:
        r = [list([list(c)]) for c in contour]
        ret.extend([numpy.array(r, dtype=numpy.int32)])
    return ret

def convContoursS(contours):
    '''
    convert OpenCV contours to SimpleCV contours
    '''
    ret = []
    for contour in contours:
        c = []
        for p in contour:
            c.append(tuple(p[0]))
        ret.append(c)
    return ret

def convImageO(image):
    '''
    convert SimpleCV image to OpenCV image
    '''
    return image.getNumpyCv2()

def convImageS(image, rgb=True):
    '''
    convert OpenCV image to SimpleCV image
    '''
    assert have_simplecv == True # function requires SimpleCV
    if len(image.shape) == 3:
        # rgb or hsv, etc.
        if rgb == True:
            image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
        image = numpy.transpose(image, (1, 0, 2))
    else:
        image = numpy.transpose(image, (1, 0))
    return Image(image)

#
# Basic blob/contour functions
#

def createBlobs(img, contours):
    '''
    convert SimpleCV contours to SimpleCV blobs
    '''
    try:
        w, h = img.size()
    except TypeError: # OpenCV Image
        img = convImageS(img)
        w, h = img.size()
    # create mask
    oc = convContoursO(contours)
    oi = numpy.zeros((h, w), dtype=numpy.uint8)
    cv2.drawContours(oi, oc, -1, 255, -1)
    ci = convImageS(oi)
    # create blobs
    blobs = img.findBlobsFromMask(ci)
    return blobs

def createMask(dim, contours, val=255):
    '''
    convert SimpleCV contours to BW image mask
    @param  dim      (h, w) image dimensions
    @param  contours SimpleCV contours
    @param  val      value of masked pixels
    @return img      masked BW image
    '''
    img = numpy.zeros((dim[0], dim[1]), dtype=numpy.uint8)
    for contour in contours:
        oc = convContoursO([contour])
        cv2.drawContours(img, oc, -1, val, -1)
    return img

def createCircularMask(img_sz, contours, x, y, rad):
    '''
    createMask with an additional circular mask applied
    '''
    mask = createMask(img_sz, contours)
    h, w = mask.shape
    cir = numpy.zeros((h, w), dtype=numpy.uint8)
    cv2.circle(cir, (int(x), int(y)), rad, 255, -1)
    ret = numpy.minimum(mask, cir)
    return ret

def createContours(mask, min_area=-1, exclude_zero=True):
    '''
    extract SimpleCV contours from image mask
    treat each value in mask as a separate mask
    if exclude_zero == True, do not make zero values into a mask
    sort the output contours by area
    '''
    uniq_vals = list(set(mask.ravel()))
    ht, wt = mask.shape[:2]
    cont_all = []
    for v in uniq_vals:
        if exclude_zero == True and v == 0:
            continue
        m = numpy.zeros((ht, wt), dtype=numpy.uint8)
        elem = numpy.where(mask == v)
        m[elem] = 255
        cont, hier = cv2.findContours(m, cv2.RETR_EXTERNAL,\
                                      cv2.CHAIN_APPROX_SIMPLE)
        cont_all.extend(convContoursS(cont))

    if len(cont_all) == 0:
        return []

    # if there are contours, try and sort them by area
    cont_db = []
    for c in cont_all:
        area = getContourArea(c)
        if min_area == -1:
            cont_db.append((area, c))
        elif area > min_area:
            cont_db.append((area, c))
    cont_db.sort(key=lambda x: x[0], reverse=True)
    if len(cont_db) > 0:
        return zip(*cont_db)[1]
    return []

def getContourArea(contour):
    '''
    get contour area from SimpleCV contour
    '''
    return cv2.contourArea(convContoursO([contour])[0])

if __name__ == '__main__':
    main()

#
# Complex blob/contour functions
#

def extractExternalContours(bim):
    '''
    fill in internal contours, return external contours
    @param  bim      binary thresholded image
    @return contours SimpleCV external contours, largest area first
    '''

    # Fill in internal contours
    cont, hier = cv2.findContours(numpy.array(bim),\
                                  cv2.RETR_CCOMP, cv2.CHAIN_APPROX_SIMPLE)
    for c in cont:
        cv2.drawContours(bim, [c], 0, 255, -1)

    # Find extrnal contours
    cont, hier = cv2.findContours(numpy.array(bim),\
                                  cv2.RETR_CCOMP, cv2.CHAIN_APPROX_SIMPLE)
    contour_pair = []
    scont = convContoursS(cont)
    for sc in scont:
        c = convContoursO([sc])
        a = cv2.contourArea(c[0])
        contour_pair.append((a, sc))
    contour_pair.sort(key=lambda x: x[0], reverse=True)
    sz, ct = zip(*contour_pair)
    contours = list(ct)
    return contours
