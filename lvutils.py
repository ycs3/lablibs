import numpy
import cPickle
import os
import cv2
import math

colors = [(0, 0, 255), (0, 255, 0), (255, 0, 0), (0, 255, 255), (255, 0, 255), (255, 255, 0), (0, 128, 255), (128, 0, 255), (128, 255, 0), (0, 255, 128), (255, 0, 128), (255, 128, 0)]
for i in range(256):
    _r = numpy.random.randint(0, 256)
    _g = numpy.random.randint(0, 256)
    _b = numpy.random.randint(0, 256)
    colors.append((_r, _g, _b))

class Pickle(object):
    ''' simplified interface to cPickle '''
    def __init__(self, filename):
        self.filename = filename
        self.data = None
        try:
            fp = open(self.filename, "rb")
            self.data = cPickle.load(fp)
            fp.close()
        except IOError:
            pass
        except TypeError:
            pass
        except cPickle.UnpicklingError:
            pass
        except EOFError:
            pass

    def save(self, data=None):
        if self.filename == None:
            return False
        if data != None:
            self.data = data
        fp = open(self.filename, "wb")
        cPickle.dump(self.data, fp, cPickle.HIGHEST_PROTOCOL)
        fp.close()
        return True

    def load(self):
        return self.data

class ListQueue(object):
    ''' queue wrapper '''
    def __init__(self, max_size=5):
        self.max_size, self.data = max_size, []
        self.p, self.c = None, None

    def push(self, d):
        self.data.append(d)
        if len(self.data) > self.max_size:
            self.data = self.data[1:]
        self.p = self.get(-2)
        self.c = self.get(-1)
    
    def add(self, d):
        self.push(d)

    def get(self, i):
        # get -1, -2, ...
        try:
            return self.data[i]
        except IndexError:
            return self.data[0]

    def dump(self):
        print self.data

    def len_(self):
        return len(self.data)

    def full(self):
        if self.max_size == len(self.data):
            return True
        return False

def add_dict_elem(dict_, elem_list):
    d = dict_
    for e in elem_list[:-2]:
        try:
            _ = d[e]
        except KeyError:
            d[e] = {}
        d = d[e]
    d[elem_list[-2]] = elem_list[-1]
    return dict_

class DictArray(object):
    ''' dictionary of arrays '''
    def __init__(self):
        self.data = {}

    def append(self, key, elem):
        try:
            _ = self.data[key]
        except KeyError:
            self.data[key] = []
        self.data[key].append(elem)

    def add(self, key, elem):
        self.append(key, elem)

    def get(self, key):
        try:
            ret = self.data[key]
        except KeyError:
            ret = None
        return ret

#
# image manipulation
#

def imnorm(img, max=None, offset=False):
    img_f = numpy.array(img, dtype=numpy.float)
    if max == None:
        m = numpy.max(img)
    else:
        m = max
    if m != 0:
        if offset:
            seg_z = numpy.where(img == 0)
            ret = (img_f/m)/2+.5
            ret[seg_z] = 0
        else:
            ret = img_f/m
    else:
        ret = img_f
    return ret

def imgray(img):
    return cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

def color_depthmap_old(depth_data):
    depth = numpy.array((depth_data/5000.)*255, dtype=numpy.uint8)
    return depth

def color_depthmap(depth):
    numpy.clip(depth, 0, 2**10 - 1, depth)
    depth >>= 2
    depth = depth.astype(numpy.uint8)
    return depth

def image_resize(img, resize):
    ht, wt = img.shape[:2]
    if resize != None:
        if resize[0] != wt or resize[1] != ht:
            img = cv2.resize(img, (resize[0], resize[1]))
    return img

def imshow(label, img, show=True, wait=10):
    if show:
        cv2.imshow(label, img)
        k = cv2.waitKey(wait)
        return k

def imclick(img, name=None, click=None):
    if name == None:
        name = "IMG_" + str(time.time())
    cv2.namedWindow(name)
    cv2.setMouseCallback(name, _getimshowpt, (click,))
    cv2.imshow(name, img)
    return cv2.waitKey()

#
# file manipulation
#

def get_filename(path):
    return os.path.splitext(os.path.basename(path))[0]

def get_path(path):
    if path == None:
        return None
    return path[:-1] if path[-1] == "/" else path

def get_trans(trans):
    trans = trans.split()
    return [float(t[:-1]) if t[-1] == ',' else float(t) for t in trans]

def get_properties(filename):
    properties = {}
    properties['trans'] = trans_default
    try:
        with open(filename) as f:
            for line in f.readlines():
                key, var = line.strip().split(":", 1)
                #print key, var
                var = var.strip()
                if key == "trans":
                    properties[key] = get_trans(var)
                else:
                    properties[key] = var
    except IOError:
        print "ERROR: Can't find properties file. Using default."
    return properties

def get_user_properties(data_dir, properties_file=None):
    data_dir = get_path(data_dir)
    if properties_file == None:
        properties_file = data_dir + "/properties.txt"
    return get_properties(properties_file)

#
# distance functions
#

def euclidean_dist(p1, p2):
    s = 0.
    for k in range(len(p1)):
        d2 = (p1[k]-p2[k])*(p1[k]-p2[k])
        s += d2
    return math.sqrt(s)

def dist_3d(p1, p2):
    x1, y1, z1 = p1
    x2, y2, z2 = p2
    return math.sqrt((x2-x1)*(x2-x1) + (y2-y1)*(y2-y1) + (z2-z1)*(z2-z1))

def closeto_3d(v1, v2, thresh=.02):
    if dist_3d(v1, v2) < thresh:
        return True
    return False

#
# argument parsing utility functions
#

def arglist_int(s):
    try:
        return map(int, s.split(','))
    except:
        raise argparse.ArgumentTypeError("Argument must be integers separated by a comma")

def arglist_float(s):
    try:
        return map(float, s.split(','))
    except:
        raise argparse.ArgumentTypeError("Argument must be integers separated by a comma")

def parse_step(step):
    if step == None:
        return 0, 99999, 1
    elif len(step) == 1 and step[0] >= 0:
        return 0, 99999, step[0]
    elif len(step) == 1 and step[0] < 0:
        return 99999, 0, step[0]
    elif len(step) == 2:
        return step[0], step[1], 1
    elif len(step) > 2:
        return step[0], step[1], step[2]
    return 0, 99999, 1

def _getimshowpt(event, x, y, flags, param):
    if param[0] != None and event == cv2.EVENT_LBUTTONDBLCLK:
        param[0](x ,y)

#
# color conversion
#

def lab2bgr(l, a, b):
    timg = numpy.zeros((1, 1, 3), dtype=numpy.uint8)
    timg[0][0][0], timg[0][0][1], timg[0][0][2] = l, a, b
    tval = cv2.cvtColor(timg, cv2.COLOR_LAB2BGR)[0][0]
    b_, g_, r_ = int(tval[0]), int(tval[1]), int(tval[2])
    return b_, g_, r_

def bgr2lab(b_, g_, r_):
    timg = numpy.zeros((1, 1, 3), dtype=numpy.uint8)
    timg[0][0][0], timg[0][0][1], timg[0][0][2] = b_, g_, r_
    tval = cv2.cvtColor(timg, cv2.COLOR_BGR2LAB)[0][0]
    l, a, b = int(tval[0]), int(tval[1]), int(tval[2])
    return l, a, b

def hsv2bgr(h, s, v):
    timg = numpy.zeros((1, 1, 3), dtype=numpy.uint8)
    timg[0][0][0], timg[0][0][1], timg[0][0][2] = h, s, v
    tval = cv2.cvtColor(timg, cv2.COLOR_HSV2BGR)[0][0]
    b_, g_, r_ = int(tval[0]), int(tval[1]), int(tval[2])
    return b_, g_, r_

def bgr2hsv(b_, g_, r_):
    timg = numpy.zeros((1, 1, 3), dtype=numpy.uint8)
    timg[0][0][0], timg[0][0][1], timg[0][0][2] = b_, g_, r_
    tval = cv2.cvtColor(timg, cv2.COLOR_BGR2HSV)[0][0]
    h, s, v = int(tval[0]), int(tval[1]), int(tval[2])
    return h, s, v

#
# drawing helpers
#

def putText(img, color, x, y, text="", sz=.5):
    cv2.putText(img, str(text), (int(x), int(y)), cv2.FONT_HERSHEY_COMPLEX, sz, color)

def circle(img, color, x, y, r, thickness=-1):
    cv2.circle(img, (int(x), int(y)), r, color, thickness)

def rectangle(img, color, x1, y1, x2, y2, thickness=1):
    cv2.rectangle(img, (int(x1), int(y1)), (int(x2), int(y2)), color, thickness)

def line(img, color, x1, y1, x2, y2, thickness=1):
    cv2.line(img, (int(x1), int(y1)), (int(x2), int(y2)), color, thickness)

def _convContoursO(contours):
    '''
    convert SimpleCV (list of) contours to OpenCV contours
    '''
    ret = []
    for contour in contours:
        r = [list([list(c)]) for c in contour]
        ret.extend([numpy.array(r, dtype=numpy.int32)])
    return ret

def contour(img, color, contour, thickness=1):
    c = _convContoursO([contour])
    cv2.drawContours(img, c, -1, color, thickness)

def contour_com(contour):
    c = _convContoursO([contour])
    M = cv2.moments(c[0])
    cx = int(M['m10']/M['m00'])
    cy = int(M['m01']/M['m00'])
    return cx, cy

def contour_dist(contour1, contour2):
    nc1, nc2 = len(contour1), len(contour2)
    if nc1 > nc2:
        contour1, contour2 = contour2, contour1
        nc1, nc2 = nc2, nc1

    oc2 = _convContoursO([contour2])[0]
    dist = []
    for c in contour1:
        d = cv2.pointPolygonTest(oc2, c, True)
        dist.append(d)
    dist.sort()
    return abs(dist[-1])

def contour_match(contour1, contour2):
    oc1 = _convContoursO([contour1])[0]
    oc2 = _convContoursO([contour2])[0]
    return cv2.matchShapes(oc1, oc2, 1, 0.)

