import cv2
import numpy
import compatcv # uses SimpleCV
from lvutils import Pickle

have_scipy = False
try:
    from scipy import weave
    have_scipy = True
except ImportError:
    pass

have_sklearn = False
try:
    import sklearn.mixture
    have_sklearn = True
except ImportError:
    pass


import warnings
warnings.simplefilter(action = "ignore", category = FutureWarning)
warnings.simplefilter(action = "ignore", category = DeprecationWarning)

#
# Internal helper functions
#

def _nchannels(img):
    '''
    get the number o
    '''
    if img == None:
        return 0
    if len(img.shape) < 3:
        return 1
    return img.shape[2]

#def _checkImage(img, channels=None, mask=None, conv=None):
def _checkImage(img, channels=None, conv=None):
    '''
    Handles preprocessing for images
    @param  img       1 or 3 channel image
    @param  channels  only use selected, reorder channels if specified
    @param  conv      cv2.cv.CV_*/cv2.COLOR_* (BGR/RGB/GRAY/HSV/Lab/Luv etc)
    @return img       converted, reordered image
    '''
    if conv != None:
        img = cv2.cvtColor(img, conv)
    img = rearrangeChannels(img, channels)

    return img

def _getHistBins(channels, bins):
    ret = []
    if bins == None:
        bins = 256
    if not isinstance(bins, (list, tuple)):
        for i in range(len(channels)):
            ret += [bins]
    return ret

def _defaultHistRanges(channels):
    ret = []
    for i in range(len(channels)):
        ret += [0, 256]
    return ret

#
# colorList and dependencies
#

def cvtColorList(arr, code):
    '''
    cv2.cvtColor, but input is list/array of tuples
    '''
    arr_ = numpy.array([arr], dtype=numpy.uint8)
    arr = cv2.cvtColor(arr_, code)[0]
    return arr

def subChannelsList(arr, channels):
    '''
    select a subset of channels from list of tuples
    TODO: Possibly use cv2.mixChannels
    '''
    tgt = [arr[:,channel] for channel in channels]
    return numpy.vstack(tgt).T

def subChannelsImage(img, channels):
    '''
    TODO: Possibly use cv2.mixChanels instead
    '''
    tgt = [img[:,:,channel].ravel() for channel in channels]
    return numpy.vstack(tgt).T

def colorList(img, channels=None, mask=None, conv=None):
    '''
    create a list of tuples from image

    @param img      3 channel image
    @param channels use selected channels (ex: [0, 2])
    @param mask     single channel mask (0/255 values)
    @param conv     cv2.cv.CV_* or cv2.COLOR_*
    @return         list of tuples created from image
    @retval img_lst list of tuples
    '''
    h, w = img.shape[0], img.shape[1]
    img_lst = img.ravel().reshape((h*w, 3)) # unravel, (w*h, 3)
    if mask != None:
        idx_lst = numpy.where(mask.ravel() == 255)[0] # mask indices
        img_lst = img_lst[idx_lst] # select mask
    if conv != None:
        img_lst = cvtColorList(img_lst, conv) # convert colorspace
    if channels != None:
        img_lst = subChannelsList(img_lst, channels) # select channels
    return img_lst

#
# Data functions
#

def read_aslist(filename, valtype=float):
    '''
    read each whitespace delimited line of values as tuples
    numpy array of shape (# line, # vals per line)
    '''
    vals = []
    for line in open(filename).readlines():
        line = line.strip()
        if len(line) == 0:
            continue
        if line.startswith("//"):
            continue
        line = line.split()
        line_arr = []
        for v in line:
            line_arr.append(valtype(float(v)))
        vals.append(line_arr)
    return vals

def read_asarr(filename, valtype=float):
    ret = read_aslist(filename, valtype)
    return numpy.array(ret, dtype=numpy.float32)

def read_asimg(filename, valtype=float):
    ret = read_asarr(filename, valtype)
    return numpy.array([ret], dtype=numpy.uint8)

def list_asimg(lst):
    return numpy.array([lst], dtype=numpy.uint8)

def masked_asarr(img, mask, valtype=float):
    idxs = numpy.where(mask != 0)
    return numpy.array(img[idxs], dtype=numpy.float32)

def read_ascontour(filename):
    '''
    read contour information from data captured by contour.py
    return SimpleCV contours
    '''
    contours = []
    for line in open(filename).readlines():
        if line.startswith("//"):
            line = line.strip().split(":")[1]
            line = line.split(" ")
            contour = []
            for elem in line:
                elem = elem[1:-1].split(",")
                x, y = elem[0], elem[1]
                contour.append((int(x), int(y)))
            if len(contour) > 0:
                contours.append(contour)
    return contours

#
# Utility functions
#

def rearrangeChannels(img, channels):
    '''
    rearranges channels: BGR image with [1, 0, 2] creates GBR
    '''
    if channels == None:
        return img
    #if _nchannels(img) == 1:
    #    return img
    if len(channels) == 1:
        ret = numpy.empty((img.shape[0], img.shape[1]), dtype=img.dtype)
        ret = img[:,:,channels[0]]
        return ret
    ret = numpy.empty((img.shape[0], img.shape[1], len(channels)),\
                      dtype=img.dtype)
    if _nchannels(img) == 1:
        for i, c in enumerate(channels):
            ret[:,:,i] = img
    else:
        for i, c in enumerate(channels):
            ret[:,:,i] = img[:,:,c]
    return ret

def thresholdImage(mask, value=0, use_minimum=False, write_val=255):
    '''
    threshold image, create a single-channel mask
    @param  mask        input mask (can be multiple channels, 8/32-bit)
    @param  value       if src > value: 255; else: 0
    @param  use_minimum use minimum value from rgb for mask image
    @param  write_val   use this value to write for masked values
    @return thresh      thresholded mask image
    '''
    if mask == None:
        return None
    if mask.dtype != numpy.uint8 and mask.dtype != numpy.float32:
        mask = numpy.array(mask, dtype=numpy.float32)
    if _nchannels(mask) > 1:
        # combine all channels
        p = mask[:,:,0]
        for i in range(1, _nchannels(mask)):
            if use_minimum == True:
                p = numpy.minimum(p, mask[:,:,i])
            else:
                p = numpy.maximum(p, mask[:,:,i])
        mask = p
    _, thresh = cv2.threshold(mask, value, write_val, cv2.THRESH_BINARY)
    if numpy.dtype != numpy.uint8:
        thresh = numpy.array(thresh, dtype=numpy.uint8)
    return thresh

def invertImage(img):
    return cv2.bitwise_not(img)

def erode_dilate(a, repeat=1):
    for i in range(repeat):
        a = cv2.erode(a, None)
    for i in range(repeat):
        a = cv2.dilate(a, None)
    return a

def erode(a, repeat=1):
    for i in range(repeat):
        a = cv2.erode(a, None)
    return a

def dilate(a, repeat=1):
    for i in range(repeat):
        a = cv2.dilate(a, None)
    return a

def maskImage(img, mask):
    img = numpy.array(img, copy=True)
    if mask.dtype == numpy.float:
        ht, wt = mask.shape[:2]
        m = numpy.zeros((ht, wt), dtype=numpy.uint8)
        m[numpy.where(mask != 0)] = 255
        mask = m
    if _nchannels(img) > 1:
        for i in range(_nchannels(img)):
            img[:,:,i] = numpy.minimum(img[:,:,i], mask)
    else:
        img = numpy.minimum(img, mask)
    return img

def imageFloatToInt(img):
    return numpy.array(img * 255, dtype=numpy.uint8)

#
# getCVHist related
#

def _get2DCVHist(hist, vert=0, hori=1):
    '''
    get a 2D histogram from a 3D histogram
    '''
    shape = hist.shape
    axes = [0, 1, 2]
    vert_shape = shape[vert]
    hori_shape = shape[hori]
    axes.remove(vert)
    axes.remove(hori)
    del_axes = axes[0]
    del_shape = shape[del_axes]
    t = numpy.zeros((vert_shape, hori_shape))
    for j in range(vert_shape):
        for i in range(hori_shape):
            for k in range(del_shape):
                if vert == 0 and hori == 1 and del_axes == 2:
                    t[j][i] += hist[j][i][k]
                elif vert == 0 and hori == 2 and del_axes == 1:
                    t[j][i] += hist[j][k][i]
                elif vert == 1 and hori == 0 and del_axes == 2:
                    t[j][i] += hist[i][j][k]
                elif vert == 1 and hori == 2 and del_axes == 0:
                    t[j][i] += hist[k][j][i]
                elif vert == 2 and hori == 0 and del_axes == 1:
                    t[j][i] += hist[i][k][j]
                elif vert == 2 and hori == 1 and del_axes == 0:
                    t[j][i] += hist[k][i][j]
    return t

def _drawCVHist_single(hist, resize):
    raw = hist / numpy.max(hist)
    if resize != None:
        raw = cv2.resize(raw, resize, interpolation=cv2.INTER_NEAREST)
    return raw

def getCVHist(hist, resize=None, show=False):
    '''
    draw an OpenCV histogram
    '''
    hist_im = None
    dim = len(hist.shape)
    if dim == 2 and hist.shape[1] == 1:
        histT = numpy.transpose(hist)
        raw = _drawCVHist_single(histT, resize)
        if show:
            cv2.imshow('HIST V:0', raw)
        hist_im = raw
    elif dim == 2:
        raw = _drawCVHist_single(hist, resize)
        if show:
            cv2.imshow('HIST V:0, H:1', raw)
        hist_im = raw
    elif dim == 3:
        r = _get2DCVHist(hist, 0, 1)
        r = _drawCVHist_single(r, resize)
        if show:
            cv2.imshow('HIST V:0, H:1 (BGR->BG)', r)
        g = _get2DCVHist(hist, 0, 2)
        g = _drawCVHist_single(g, resize)
        if show:
            cv2.imshow('HIST V:0, H:2 (BGR->BR)', g)
        b = _get2DCVHist(hist, 1, 2)
        b = _drawCVHist_single(b, resize)
        if show:
            cv2.imshow('HIST V:1, H:2 (BGR->GR)', b)
        hist_im = numpy.dstack((b, g, r))
    if show:
        cv2.waitKey()
    return hist_im

#
# getArrHist related
#

def _drawArrHist_single(arr, vert_range=256, hori_range=256,\
                        vert=0, hori=1, resize=None):
    # draw a histogram (0-vert_range, 0-hori_trange) with two channels
    # from arr tuple
    raw = numpy.zeros((vert_range, hori_range), dtype=numpy.float)
    for p in arr:
        raw[p[vert]][p[hori]] += 1.
    raw = raw / numpy.max(raw)
    if resize != None:
        raw = cv2.resize(raw, resize, interpolation=cv2.INTER_NEAREST)
    return raw

def _convHistToArr(hist):
    '''
    expand histogram to tuple of points, converted to numpy array
    TODO: make efficient
    debugging purposes only
    '''
    dim = len(hist.shape)
    if dim == 1:
        r = []
        a = hist.shape[0]
        for i in range(a):
            r += [i for v in range(hist[i])]
    if dim == 2:
        r = []
        b, a = hist.shape[0:2]
        for j in range(b):
            for i in range(a):
                r += [(j, i) for v in range(hist[j, i])]
    if dim == 3:
        r = []
        c, b, a = hist.shape[0:3]
        for k in range(c):
            for j in range(b):
                for i in range(a):
                    r += [(k, j, i) for v in range(hist[k, j, i])]
    return numpy.array(r)

def getArrHist(arr, resize=None, bins=256, show=False):
    dim = len(arr[0])
    bins = _getHistBins(arr[0], bins)
    hist_im = None
    if dim == 2:
        raw = _drawArrHist_single(arr, bins[0], bins[1], 0, 1, resize)
        if show:
            cv2.imshow('HIST V:0, H:1', raw)
        hist_im = raw
    elif dim == 3:
        r = _drawArrHist_single(arr, bins[0], bins[1], 0, 1, resize)
        if show:
            cv2.imshow('HIST V:0, H:1', r)
        g = _drawArrHist_single(arr, bins[0], bins[2], 0, 2, resize)
        if show:
            cv2.imshow('HIST V:0, H:2', g)
        b = _drawArrHist_single(arr, bins[1], bins[2], 1, 2, resize)
        if show:
            cv2.imshow('HIST V:1, H:2', b)
        hist_im = numpy.dstack((b, g, r))
    else:
        pass
    if show:
        cv2.waitKey()
    return hist_im

#
# Color Histogram
#

def trainColorGMM(true_pts, false_pts, convert=None, channels=None,\
                  true_nmix=1, false_nmix=1):
    assert have_sklearn == True # function requires sklearn
    if convert != None:
        true_pts = cvtColorList(true_pts, convert)
        false_pts = cvtColorList(false_pts, convert)
    if channels != None:
        true_pts = subChannelsList(true_pts, channels)
        false_pts = subChannelsList(false_pts, channels)
    true_mix = sklearn.mixture.GMM(true_nmix, 'full')
    false_mix = sklearn.mixture.GMM(false_nmix, 'full')
    true_mix.fit(true_pts)
    false_mix.fit(false_pts)
    mix = (true_mix, false_mix)
    return mix

def detectColorGMM(im, mix, convert=None, channels=None):
    true_mix, false_mix = mix
    rows, cols, n_ch = im.shape
    if convert != None:
        im = cv2.cvtColor(im, convert)
    if channels != None:
        data = subChannelsImage(im, channels)
        n_ch = len(channels)
    p1_ = numpy.exp(true_mix.eval(data)[0])
    p1 = p1_.reshape(rows, cols)
    p2_ = numpy.exp(false_mix.eval(data)[0])
    p2 = p2_.reshape(rows, cols)
    p3 = numpy.power(numpy.ones((rows, cols))/256, n_ch)
    ret = numpy.array(p1 / (p1 + p2 + p3), dtype=numpy.float32)
    return ret

def trainColorGMMSingle(pts, convert=None, channels=None, nmix=1):
    assert have_sklearn == True # function requires sklearn
    if convert != None:
        pts = cvtColorList(pts, convert)
    if channels != None:
        pts = subChannelsList(pts, channels)
    mix = sklearn.mixture.GMM(nmix, 'full')
    mix.fit(pts)
    return mix

def detectColorGMMSingle(im, mix, convert=None, channels=None):
    rows, cols, n_ch = im.shape
    if convert != None:
        im = cv2.cvtColor(im, convert)
    if channels != None:
        data = subChannelsImage(im, channels)
        n_ch = len(channels)
    else:
        n_ch = _nchannels(im)
        data  = subChannelsImage(im, range(n_ch))
    p1_ = numpy.exp(mix.eval(data)[0])
    p1 = p1_.reshape(rows, cols)
    ret = numpy.array(p1, dtype=numpy.float32)
    return ret


class ColorGMM(object):
    def __init__(self, true_pts, false_pts, convert=None, channels=None, true_nmix=1, false_nmix=1):
        self.convert = convert
        self.channels = channels
        self.mix = trainColorGMM(true_pts, false_pts, convert, channels, true_nmix, false_nmix)

    def detect(self, im, threshold=None):
        ret = detectColorGMM(im, self.mix, self.convert, self.channels)
        if threshold != None:
            ret = thresholdImage(ret, threshold)
        return ret

class ColorGMMSingle(object):
    def __init__(self, pts, convert=None, channels=None, nmix=1):
        ''' pts can be a list of points, or a filename '''
        self.convert = convert
        self.channels = channels
        self.nmix = nmix
        if isinstance(pts, basestring):
            # pts is a file name
            pts = read_asarr(pts)
        self.orig_pts = pts
        self.pts = pts
        self.mix = trainColorGMMSingle(self.pts, convert, channels, nmix)

    def refit(self, pts):
        ''' refit with original points and recent points '''
        self.pts = numpy.append(self.pts, pts, axis=0)[-10000:]
        self.pts = numpy.append(self.orig_pts, self.pts, axis=0)
        self.mix = trainColorGMMSingle(self.pts, self.convert, self.channels, self.nmix)

    def detect(self, im, threshold=None):
        ret = detectColorGMMSingle(im, self.mix, self.convert, self.channels)
        if threshold != None:
            ret = thresholdImage(ret, threshold)
        return ret

    def detect_pixel(self, rgb):
        r, g, b = rgb
        color = numpy.array([[[b, g, r]]], dtype=numpy.uint8)
        ret = self.detect(color)[0][0]
        return ret

    def save(self, filename):
        ''' save object to a file '''
        p = Pickle(filename)
        p.save(self)

def colorHist(img, channels=None, mask=None, conv=None, bins=None,\
              ranges=None, prevHist=None):
    '''
    compute histogram
    ex) obj.get_colorhist(im,[1,2],None,"Lab",64,[0,256,0,256],"BGR")
    @param  img       1 or 3 channel image
    @param  channels  only use selected, rearrange
    @param  mask      single channel mask (0/255 values)
    @param  conv      cv2.cv.CV_* or cv2.COLOR_*
    @param  bins      number of bins for histogram
    @param  ranges    (optional) range of histogram (0-value)
    @param  prevHist  accumulate previous histogram
    @return hist      return OpenCV histogram (n-dim numpy array)
    '''
    img = _checkImage(img, channels, conv)
    mask = thresholdImage(mask)

    # override channels, has been changed by _checkImage
    channels = range(_nchannels(img))

    bins = _getHistBins(channels, bins)
    if ranges == None:
        ranges = _defaultHistRanges(channels)

    hist = None
    if prevHist == None:
        hist = cv2.calcHist([img], channels, mask, bins, ranges)
    else:
        hist = cv2.calcHist([img], channels, mask, bins, ranges,\
                            prevHist, True)
    return hist

def detectColorHist(img, hist, channels=None, conv=None, bins=None,\
                    ranges=None):
    '''
    calcBackProject doesn't seem to work with three channels...??
    http://stackoverflow.com/questions/26128615/cv2-calcbackproject-fails-for-3d-histograms
    use only two for accurate results
    '''
    img = _checkImage(img, channels, conv)
    channels = range(_nchannels(img))

    if ranges == None:
        ranges = _defaultHistRanges(channels)

    # output for backproject is the same dtype as input
    img = numpy.array(img, dtype=numpy.float32)

    bp = cv2.calcBackProject([img], channels, hist, ranges, 1.)
    # set max to 1
    #bp = numpy.array(bp, dtype=numpy.float) / float(numpy.max(bp))
    return bp

def detectColorHistBGR(img, hist, bins=None):
    '''
    calcBackProject specifically for BGR images
    '''
    assert have_scipy == True # function requires scipy
    h, w = img.shape[:2]
    binsz = int(256/bins);
    out = numpy.zeros((h, w), dtype=numpy.float)
    code = r"""
    int imbase;
    float hval;
    unsigned char b, g, r, b_, g_, r_;
    for(int j = 0; j < h; j += 1) {
        for(int i = 0; i < w; i += 1) {
            imbase = j*w+i;
            b = ((int)(img[imbase*3+0]));
            g = ((int)(img[imbase*3+1]));
            r = ((int)(img[imbase*3+2]));
            b_ = b / binsz;
            g_ = g / binsz;
            r_ = r / binsz;
            int v = b_*bins*bins+g_*bins+r_;
            hval = hist[v];
            out[imbase] = hval;
        }
    }
    """
    weave.inline(code, ['img', 'hist', 'h', 'w', 'bins', 'binsz', 'out'])
    return out
