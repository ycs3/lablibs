import numpy
import scipy.weave as weave
import math

import numpy as np

from laceconfig import show_graphics
if show_graphics:
    from OpenGL.GL import *
    from OpenGL.GLU import *
    from OpenGL.GLUT import *

# from pyKinectTools/utils/DepthUtils.py
fx_d = 1.0 / 574
fy_d = 1.0 / 574
cx_d = 240.
cy_d = 320.

fx_rgb = 1./5.2921508098293293e+02
fy_rgb = 1./5.2556393630057437e+02
cy_rgb = 3.2894272028759258e+02
cx_rgb = 2.6748068171871557e+02

# Transform between depth and color
R = np.array([[ 9.9984628826577793e-01,\
                1.2635359098409581e-03,\
               -1.7487233004436643e-02],\
              [-1.4779096108364480e-03,\
                9.9992385683542895e-01,\
               -1.2251380107679535e-02],\
              [ 1.7470421412464927e-02,\
                1.2275341476520762e-02,\
                9.9977202419716948e-01 ]])

T = np.array([  1.9985242312092553e-02,\
               -7.4423738761617583e-04,\
               -1.0916736334336222e-02 ])*1000.

def skel2depth_py(skel, rez=(480,640)):
    if type(skel) != np.ndarray:
        skel = np.array(skel)

    mod_x = rez[0]/480.
    mod_y = rez[1]/640.

    # 3d_d->3d_r
    pts_r = np.dot(R, skel.T).T + T
    # 3d_r->rgb
    rgb_x = pts_r[:,0]/fx_rgb / pts_r[:,2] + cx_rgb
    rgb_y = pts_r[:,1]/fy_rgb / pts_r[:,2] + cy_rgb
    rgb_z = pts_r[:,2]

    rgb_x *= rez[1]/640.
    rgb_y *= rez[0]/480.

    pts = np.round([rgb_x,rez[0]-rgb_y,rgb_z]).astype(np.int16).T

    return pts

def depth2world_py(pts, rez=(480,640)):
    ''' Convert depth coordinates to world coordinates
        standard Kinect calibration '''
    assert type(pts) == np.ndarray, "Wrong type into depth2world"

    pts = pts.astype(np.float)
    pts[:,0] *= 480./rez[0]
    pts[:,1] *= 640./rez[1]

    y = np.array(pts[:,1])
    x = np.array(pts[:,0])
    d = np.array(pts[:,2])

    xo =  ((x - cx_d) * d * fx_d)
    yo = ((y - cy_d) * d * fy_d)
    zo = d

    return np.round([xo, yo, zo]).astype(np.int16).T

inline_depth2skel = r"""
void
_depth2skel(int i, int j, unsigned short d, int w, int h,\
           double* x, double* y, double* z)
{
    double ni, nj, fX, fY, fZ;

    ni = (double)i * (320./(double)w);
    nj = (double)j * (240./(double)h);
    fZ = (double)d/1000.;
    fX = (ni/320. - .5) * (3.501e-3 * fZ) * 320;
    fY = (.5 - nj/240.) * (3.501e-3 * fZ) * 240;

    *x = fX; *y = fY; *z = fZ;
}
"""

inline_conv = r"""
inline double _rad(double deg) { return deg*(3.14159265/180.); }

void
_conv(double x, double y, double z, double deg1, double deg2, double deg3,\
      double* ox, double* oy, double* oz)
{
    double c_a, s_a, c_b, s_b, c_c, s_c;
    double fx, fy, fz, nx, ny, nz;

    nx = x; ny = y; nz = z;

    fx = nx; fy = ny; fz = nz;
    // X (deg1, c_a, s_a)
    c_a = cos(_rad(deg1)); s_a = sin(_rad(deg1));
    nx = fx; ny = c_a*fy+s_a*fz; nz = -s_a*fy+c_a*fz;

    fx = nx; fy = ny; fz = nz;
    // Y (deg2, c_b, s_b)
    c_b = cos(_rad(deg2)); s_b = sin(_rad(deg2));
    nx = c_b*fx-s_b*fz; ny = fy; nz = s_b*fx+c_b*fz;

    fx = nx; fy = ny; fz = nz;
    // Z (deg3, c_c, s_c)
    c_c = cos(_rad(deg3)); s_c = sin(_rad(deg3));
    nx = c_c*fx+s_c*fy; ny = -s_c*fx+c_c*fy; nz = fz;

    *ox = nx; *oy = ny; *oz = nz;
}
"""

def skel2depth(x, y, z, w=320., h=240.):
    w, h = float(w), float(h)
    nX, nY, nZ = x, y, z
    dX = .5+nX*(285.63/nZ)/320.
    dY = .5-nY*(285.63/nZ)/240.
    dX, dY = dX * (w/320.), dY * (h/240.)
    return int(dX*320.), int(dY*240.)


def conv(ix, iy, iz, deg1, deg2, deg3):
    nx, ny, nz = ix, iy, iz

    c_c = math.cos(math.radians(deg3))
    s_c = math.sin(math.radians(deg3))
    fx, fy, fz = nx, ny, nz
    nx, ny, nz = c_c*fx+s_c*fy, -s_c*fx+c_c*fy, fz

    c_b = math.cos(math.radians(deg2))
    s_b = math.sin(math.radians(deg2))
    fx, fy, fz = nx, ny, nz
    nx, ny, nz = c_b*fx-s_b*fz, fy, s_b*fx+c_b*fz

    c_a = math.cos(math.radians(deg1))
    s_a = math.sin(math.radians(deg1))
    fx, fy, fz = nx, ny, nz
    nx, ny, nz = fx, c_a*fy+s_a*fz, -s_a*fy+c_a*fz
    return nx, ny, nz

#
# Draw color and depthmap combined in 3D space
# Retrieve cropped image and 3D coordinate point cloud
#
def draw_scene(color, depth, trans, cutoff=-1, step=1,\
               get_crop=True, show_all=False, show_gl=True):
    h, w = color.shape[:2]
    da, db, dc, dx, dy, dz, x_min, x_max, y_min, z_max = trans
    cropped_color, pointcloud_3d = None, None
    do_crop = 1 if get_crop else 0
    if get_crop == True:
        cropped_color = numpy.zeros(color.shape, dtype=color.dtype)
        pointcloud_3d = numpy.zeros((depth.shape[0], depth.shape[1], 3),\
                                    dtype=numpy.double)

    code_gl_all = r"""
    unsigned short d;
    double cr, cg, cb, ix, iy, iz, ox, oy, oz;

    glBegin(GL_POINTS);
    glColor3f(1., 1., 1.);

    for(int j = 0; j < h; j += step) {
        for(int i = 0; i < w; i += step) {
            d = depth[j*w+i];
            if((int)d == 0) { continue; }
            if((int)d < cutoff) { continue; }

            cr = (double)color[j*w*3+i*3+2]/255.;
            cg = (double)color[j*w*3+i*3+1]/255.;
            cb = (double)color[j*w*3+i*3+0]/255.;
            _depth2skel(i, j, d, w, h, &ix, &iy, &iz);
            ix = ix + dx; iy = iy + dy; iz = iz + dz;
            _conv(ix, iy, iz, da, db, dc, &ox, &oy, &oz);

            glColor3f(cr, cg, cb);
            glVertex3f(-ox, oy, oz);

            /*
            if(-ox < x_min) { continue; }
            if(-ox > x_max) { continue; }
            if( oy < y_min) { continue; }
            if( oz > z_max) { continue; }
            */

            if(do_crop) {
                cropped_color[j*w*3+i*3+0] = color[j*w*3+i*3+0];
                cropped_color[j*w*3+i*3+1] = color[j*w*3+i*3+1];
                cropped_color[j*w*3+i*3+2] = color[j*w*3+i*3+2];

                pointcloud_3d[j*w*3+i*3+0] = -ox;
                pointcloud_3d[j*w*3+i*3+1] =  oy;
                pointcloud_3d[j*w*3+i*3+2] =  oz;
            }
        }
    }

    glEnd();
    glFlush();
    """
    code_gl = r"""
    unsigned short d;
    double cr, cg, cb, ix, iy, iz, ox, oy, oz;

    glBegin(GL_POINTS);
    glColor3f(1., 1., 1.);

    for(int j = 0; j < h; j += step) {
        for(int i = 0; i < w; i += step) {
            d = depth[j*w+i];
            if((int)d == 0) { continue; }
            if((int)d < cutoff) { continue; }

            cr = (double)color[j*w*3+i*3+2]/255.;
            cg = (double)color[j*w*3+i*3+1]/255.;
            cb = (double)color[j*w*3+i*3+0]/255.;
            _depth2skel(i, j, d, w, h, &ix, &iy, &iz);
            ix = ix + dx; iy = iy + dy; iz = iz + dz;
            _conv(ix, iy, iz, da, db, dc, &ox, &oy, &oz);

            /*
            if(-ox < x_min) { continue; }
            if(-ox > x_max) { continue; }
            if( oy < y_min) { continue; }
            if( oz > z_max) { continue; }
            */

            glColor3f(cr, cg, cb);
            glVertex3f(-ox, oy, oz);

            if(do_crop) {
                cropped_color[j*w*3+i*3+0] = color[j*w*3+i*3+0];
                cropped_color[j*w*3+i*3+1] = color[j*w*3+i*3+1];
                cropped_color[j*w*3+i*3+2] = color[j*w*3+i*3+2];

                pointcloud_3d[j*w*3+i*3+0] = -ox;
                pointcloud_3d[j*w*3+i*3+1] =  oy;
                pointcloud_3d[j*w*3+i*3+2] =  oz;
            }
        }
    }

    glEnd();
    glFlush();
    """
    code_nogl = r"""
    unsigned short d;
    double cr, cg, cb, ix, iy, iz, ox, oy, oz;

    for(int j = 0; j < h; j += step) {
        for(int i = 0; i < w; i += step) {
            d = depth[j*w+i];
            if((int)d == 0) { continue; }
            if((int)d < cutoff) { continue; }

            cr = (double)color[j*w*3+i*3+2]/255.;
            cg = (double)color[j*w*3+i*3+1]/255.;
            cb = (double)color[j*w*3+i*3+0]/255.;
            _depth2skel(i, j, d, w, h, &ix, &iy, &iz);
            ix = ix + dx; iy = iy + dy; iz = iz + dz;
            _conv(ix, iy, iz, da, db, dc, &ox, &oy, &oz);

            /*
            if(-ox < x_min) { continue; }
            if(-ox > x_max) { continue; }
            if( oy < y_min) { continue; }
            if( oz > z_max) { continue; }
            */

            if(do_crop) {
                cropped_color[j*w*3+i*3+0] = color[j*w*3+i*3+0];
                cropped_color[j*w*3+i*3+1] = color[j*w*3+i*3+1];
                cropped_color[j*w*3+i*3+2] = color[j*w*3+i*3+2];

                pointcloud_3d[j*w*3+i*3+0] = -ox;
                pointcloud_3d[j*w*3+i*3+1] =  oy;
                pointcloud_3d[j*w*3+i*3+2] =  oz;
            }
        }
    }
    """
    if show_gl and show_graphics and show_all:
        weave.inline(code_gl_all,\
                     ['color', 'depth', 'w', 'h', 'cutoff', 'step',\
                      'da', 'db', 'dc', 'dx', 'dy', 'dz',\
                      'x_min', 'x_max', 'y_min', 'z_max',\
                      'cropped_color', 'pointcloud_3d', 'do_crop'],\
                     headers=['<GL/gl.h>'],\
                     support_code=inline_depth2skel+inline_conv)
    elif show_gl and show_graphics:
        weave.inline(code_gl,\
                     ['color', 'depth', 'w', 'h', 'cutoff', 'step',\
                      'da', 'db', 'dc', 'dx', 'dy', 'dz',\
                      'x_min', 'x_max', 'y_min', 'z_max',\
                      'cropped_color', 'pointcloud_3d', 'do_crop'],\
                     headers=['<GL/gl.h>'],\
                     support_code=inline_depth2skel+inline_conv)
    else:
        weave.inline(code_nogl,\
                     ['color', 'depth', 'w', 'h', 'cutoff', 'step',\
                      'da', 'db', 'dc', 'dx', 'dy', 'dz',\
                      'x_min', 'x_max', 'y_min', 'z_max',\
                      'cropped_color', 'pointcloud_3d', 'do_crop'],\
                     support_code=inline_depth2skel+inline_conv)

    return cropped_color, pointcloud_3d
