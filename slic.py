import numpy
import cv2

have_scipy = False
try:
    from scipy import weave
    have_scipy = True
except ImportError:
    pass

def slicalg(color, S=10, output_false_color=False):
    '''
    color: BGR color image
    S: spacing between centers
    '''
    assert have_scipy == True # function requires scipy
    h, w = color.shape[:2]
    lab = cv2.cvtColor(color, cv2.COLOR_BGR2LAB)

    # initialize centers:
    # center row, col-idx / LAB color / center coord
    # add additional elements as needed here
    c = []
    jj = 0
    for j in range(S/2, h, S): # j: y-coord, jj: center row-idx
        ii = 0
        for i in range(S/2, w, S): # i: x-coord, ii: center col-idx
            lab_ = [lab[j,i,0], lab[j,i,1], lab[j,i,2]]
            c.append([ii, jj] + lab_ + [i, j])
            ii += 1
        jj += 1
    len_c = len(c)
    centers = numpy.array(c)

    # malloc temp centers
    n = len(centers[0])
    centers_ = numpy.zeros((len_c, n+1), dtype=numpy.float)

    # output image segmentation as center(real)/random(fake) color
    if output_false_color:
        fake_colors = 1
    else:
        fake_colors = 0
    fake_color_list = numpy.random.randint(256, size=(len_c, 3))

    labels = numpy.ones((h, w), dtype=numpy.int) * -1 # label map
    distance = numpy.ones((h, w), dtype=numpy.float) * -1 # inf

    slicimg = numpy.zeros((h, w, 3), dtype=numpy.uint8)

    code = r"""
    int n1 = n + 1; // length for temporary centers (centers_)

    for(int p = 0; p < 5; p += 1) { // repeat until converge

        // calculate distance to centers for +-2S bbox
        for(int k = 0; k < len_c; k += 1) { // for each center
            int idx = 0;
            // elements: get center stats
            int ii = (int)centers[k*n+idx]; idx += 1;
            int jj = (int)centers[k*n+idx]; idx += 1;
            // lab color
            float l = (float)centers[k*n+idx]; idx += 1;
            float a = (float)centers[k*n+idx]; idx += 1;
            float b = (float)centers[k*n+idx]; idx += 1;
            // center pixel
            int x = (int)centers[k*n+idx]; idx += 1;
            int y = (int)centers[k*n+idx]; idx += 1;

            // get a center +-2S bbox
            int x1 = (0 < x-S) ? x-S : 0;
            int x2 = (w < x+S) ? w : x+S;
            int y1 = (0 < y-S) ? y-S : 0;
            int y2 = (h < y+S) ? h : y+S;

            // for each point in bbox (p-prefix)
            for(int py = y1; py < y2; py += 1) {
                for(int px = x1; px < x2; px += 1) {
                    // point base
                    int base = py*w+px;

                    // lab color
                    float pl = l-((float)lab[base*3+0]);
                    float pa = a-((float)lab[base*3+1]);
                    float pb = b-((float)lab[base*3+2]);
                    float plab_dist = pl*pl+pa*pa+pb*pb;

                    float c2d_dist = (x-px)*(x-px)+(y-py)*(y-py);

                    float dD = sqrt(plab_dist + c2d_dist);
                    // no prior distance or distance is shorter
                    if(distance[base] == -1 || dD < distance[base]) {
                        // no prior distance or 
                        distance[base] = dD;
                        labels[base] = k;
                    }
                }
            }
        }

        // create temp centers (initialize)
        for(int k = 0; k < len_c; k += 1) {
            for(int l = 0; l < n1; l += 1) {
                centers_[k*n1+l] = 0.;
            }
        }

        // update to temp centers
        for(int y = 0; y < h; y += 1) {
            for(int x = 0; x < w; x += 1) {
                int base = y*w+x;
                int k = labels[base];
                if(k >= len_c) { continue; } // ??

                int idx = 2;
                centers_[k*n1+idx] += (float)lab[base*3+0]; idx += 1;
                centers_[k*n1+idx] += (float)lab[base*3+1]; idx += 1;
                centers_[k*n1+idx] += (float)lab[base*3+2]; idx += 1;
                centers_[k*n1+idx] += (float)x; idx += 1;
                centers_[k*n1+idx] += (float)y; idx += 1;

                centers_[k*n1+n] += 1.;
            }
        }

        // copy back temp centers
        for(int k = 0; k < len_c; k += 1) {
            if(centers_[k*n1+n] != 0.) {
                for(int l = 2; l < n; l += 1) {
                    centers_[k*n1+l] /= centers_[k*n1+n];
                }
            }
            for(int l = 2; l < n; l += 1) {
                centers[k*n+l] = (float)centers_[k*n1+l];
            }
        }
    }

    // create new slic output
    for(int y = 0; y < h; y += 1) {
        for(int x = 0; x < w; x += 1) {
            int base = y*w+x;
            int k = labels[base];
            if(k >= len_c) { continue; }

            if(fake_colors == 1) {
                slicimg[base*3+0] = fake_color_list[k*3+0];
                slicimg[base*3+1] = fake_color_list[k*3+1];
                slicimg[base*3+2] = fake_color_list[k*3+2];
            } else {
                slicimg[base*3+0] = (int)centers[k*n+2];
                slicimg[base*3+1] = (int)centers[k*n+3];
                slicimg[base*3+2] = (int)centers[k*n+4];
            }
        }
    }
    """
    weave.inline(code, ['lab', 'S', 'w', 'h', 'len_c', 'centers', 'centers_',\
                        'labels', 'distance', 'slicimg', 'n', 'fake_colors',\
                        'fake_color_list'],
                        headers=['<math.h>', '<stdlib.h>'])

    # reorder center data as row/column data structure
    cdata = {}
    for j in range(jj):
        cdata[j] = {}

    for c in range(len(centers)):
        i, j, l, a, b, x, y = centers[c]
        cdata[j][i] = [c, l, a, b, x, y]

    slicimg = cv2.cvtColor(slicimg, cv2.COLOR_LAB2BGR)
    #return slicimg, cdata, centers
    return slicimg, cdata

if __name__ == '__main__':
    im = cv2.imread('test.png')
    slicimg, cdata = slicalg(im, output_false_color=True)
    print cdata
    cv2.imshow('IM', slicimg)
    cv2.waitKey()
