import cv2
import numpy

def dense_flow(im1, im2, to_polar=False):
    if len(im1.shape) == 3:
        im1 = cv2.cvtColor(im1, cv2.COLOR_BGR2GRAY)
    if len(im2.shape) == 3:
        im2 = cv2.cvtColor(im2, cv2.COLOR_BGR2GRAY)
    flow = cv2.calcOpticalFlowFarneback(im1, im2, .5, 3, 15, 3, 5, 1.2, 0)
    if to_polar:
        mag, ang = cv2.cartToPolar(flow[...,0], flow[...,1])
        flow[...,0] = mag
        flow[...,1] = ang
    return flow

def flow_img(flow, max_val=None, in_polar=False):
    if in_polar == False:
        mag, ang = cv2.cartToPolar(flow[...,0], flow[...,1])
    else:
        mag = flow[...,0]
        ang = flow[...,1]
    h, w = flow.shape[:2]
    hsv = numpy.zeros((h, w, 3), dtype=numpy.uint8)
    hsv[...,0] = ang*180/numpy.pi/2
    hsv[...,1] = numpy.ones((h, w), dtype=numpy.uint8)*255
    if max_val == None:
        norm = cv2.normalize(mag, None, 0, 255, cv2.NORM_MINMAX)
    else:
        norm = numpy.array((numpy.minimum(mag, numpy.ones((h, w), dtype=numpy.float32)*max_val)/max_val)*255, dtype=numpy.uint8)
    hsv[...,2] = norm
    rgb = cv2.cvtColor(hsv, cv2.COLOR_HSV2BGR)
    return rgb
