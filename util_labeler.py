import cv2
import numpy

# tool for selecting image area
# dependencies: numpy, opencv(cv2)

lq = []
refresh = False

def _nchannels(img):
    '''
    get the number o
    '''
    if img == None:
        return 0
    if len(img.shape) < 3:
        return 1
    return img.shape[2]

def _convContoursO(contours):
    '''
    convert SimpleCV (list of) contours to OpenCV contours
    '''
    ret = []
    for contour in contours:
        r = [list([list(c)]) for c in contour]
        ret.extend([numpy.array(r, dtype=numpy.int32)])
    return ret

def _createMask(dim, contours, val=255):
    '''
    convert SimpleCV contours to BW image mask
    @param  dim      (h, w) image dimensions
    @param  contours SimpleCV contours
    @param  val      value of masked pixels
    @return img      masked BW image
    '''
    img = numpy.zeros((dim[0], dim[1]), dtype=numpy.uint8)
    for contour in contours:
        oc = _convContoursO([contour])
        cv2.drawContours(img, oc, -1, val, -1)
    return img

def _maskImage(img, mask):
    img = numpy.array(img, copy=True)
    if mask.dtype == numpy.float:
        ht, wt = mask.shape[:2]
        m = numpy.zeros((ht, wt), dtype=numpy.uint8)
        m[numpy.where(mask != 0)] = 255
        mask = m
    if _nchannels(img) > 1:
        for i in range(_nchannels(img)):
            img[:,:,i] = numpy.minimum(img[:,:,i], mask)
    else:
        img = numpy.minimum(img, mask)
    return img

def cvtColorList(arr, code):
    '''
    cv2.cvtColor, but input is list/array of tuples
    '''
    arr_ = numpy.array([arr], dtype=numpy.uint8)
    arr = cv2.cvtColor(arr_, code)[0]
    return arr

def subChannelsList(arr, channels):
    '''
    select a subset of channels from list of tuples
    TODO: Possibly use cv2.mixChannels
    '''
    tgt = [arr[:,channel] for channel in channels]
    return numpy.vstack(tgt).T

def subChannelsImage(img, channels):
    '''
    TODO: Possibly use cv2.mixChanels instead
    '''
    tgt = [img[:,:,channel].ravel() for channel in channels]
    return numpy.vstack(tgt).T

def colorList(img, channels=None, mask=None, conv=None):
    '''
    create a list of tuples from image

    @param img      3 channel image
    @param channels use selected channels (ex: [0, 2])
    @param mask     single channel mask (0/255 values)
    @param conv     cv2.cv.CV_* or cv2.COLOR_*
    @return         list of tuples created from image
    @retval img_lst list of tuples
    '''
    h, w = img.shape[0], img.shape[1]
    img_lst = img.ravel().reshape((h*w, 3)) # unravel, (w*h, 3)
    if mask != None:
        idx_lst = numpy.where(mask.ravel() == 255)[0] # mask indices
        img_lst = img_lst[idx_lst] # select mask
    if conv != None:
        img_lst = cvtColorList(img_lst, conv) # convert colorspace
    if channels != None:
        img_lst = subChannelsList(img_lst, channels) # select channels
    return img_lst

def click(event, x, y, flags, param):
    global lq, refresh
    if event == cv2.EVENT_LBUTTONDOWN:
        lq.append((x, y))
        print lq
        refresh = True

def labeler(im):
    global lq, refresh
    cv2.namedWindow('label')
    cv2.setMouseCallback('label', click, (click, ))
    #im_ = numpy.array(im, copy=True)
    im_ = None
    refresh = True
    while True:
        if refresh == True:
            refresh = False
            im_ = numpy.array(im, copy=True)
            cv2.putText(im_, "ESC:exit/S:save/R:remove pt", (10, 30), cv2.FONT_HERSHEY_COMPLEX, .5, (0, 255, 0))
            for idx in range(len(lq)-1):
                x1, y1 = lq[idx]
                x2, y2 = lq[idx+1]
                cv2.line(im_, (x1, y1), (x2, y2), (0, 255, 0), 2)
            if len(lq) > 1:
                x1, y1 = lq[-1]
                x2, y2 = lq[0]
                cv2.line(im_, (x1, y1), (x2, y2), (0, 255, 0), 1)
            for idx in range(len(lq)):
                x1, y1 = lq[idx]
                cv2.line(im_, (x1, y1), (x1, y1), (0, 0, 255), 4)
        cv2.imshow('label', im_)
        k = cv2.waitKey(10)
        if k == 27:
            exit()
        if k == 114: # R: remove last point
            if len(lq) > 0:
                lq = lq[:-1]
                print lq
                refresh = True
        if k == 115: # S: save to file
            break
        if k != -1:
            print "key", k
    return lq

def confirm_label(im, pts):
    global lq
    h, w = im.shape[:2]
    mask = _createMask((h, w), [pts])
    sel_img = _maskImage(im, mask)
    cv2.putText(sel_img, "ESC:cancel/any other key:save", (10, 30), cv2.FONT_HERSHEY_COMPLEX, .5, (0, 255, 0))
    cv2.imshow('label', sel_img)
    k = cv2.waitKey()
    if k == 27:
        lq = []
        return False, None
    return True, mask

def save_to_file(imfile, outfile, pts, clist):
    global lq
    with open(outfile, "a") as f:
        f.write("\n//BGR " + outfile + " " + imfile + " :")
        for c in pts:
            x, y = c
            f.write("(" + str(x) + "," + str(y) + ") ")
        f.write("\n")
        for l in clist:
            for v in l:
                f.write("\t%3d" % v)
            f.write("\n")
    lq = []
    print "Writing to", outfile, "done."


if __name__ == '__main__':
    import sys
    if len(sys.argv) != 3:
        print "Usage: python", sys.argv[0], "image_file output_file"
        print
        print "       labeled data will be appended to output_file"
        print "       output_file will be created if it does not exist"
        exit()
    filename = sys.argv[1]
    outfile = sys.argv[2]

    im = cv2.imread(filename)
    while True:
        pts = labeler(im)
        res, mask = confirm_label(im, pts)
        if res == True:
            lst = colorList(im, mask=mask)
            save_to_file(filename, outfile, pts, lst)
