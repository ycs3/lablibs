import cv2

# tool for extracting frames from video
# dependencies: opencv(cv2)

def get_frame(video_path, frame_no=None):
    vc = cv2.VideoCapture()
    vc.open(video_path)
    if frame_no == None:
        return vc.get(cv2.cv.CV_CAP_PROP_FRAME_COUNT)
    vc.set(cv2.cv.CV_CAP_PROP_POS_FRAMES, int(frame_no))
    ret, im = vc.read()
    return im

if __name__ == '__main__':
    import sys
    if len(sys.argv) == 2:
        path = sys.argv[1]
        print int(get_frame(path))
        exit()
    if len(sys.argv) == 3:
        path = sys.argv[1]
        frame_no = sys.argv[2]
        output_file = None
    elif len(sys.argv) == 4:
        path = sys.argv[1]
        frame_no = sys.argv[2]
        output_file = sys.argv[3]
    else: 
        print "Usage: python", sys.argv[0], "video_file [frame_no] [output_file]"
        exit()
    im = get_frame(path, frame_no)
    if output_file != None:
        cv2.imwrite(output_file, im)
    else:
        cv2.imshow('output', im)
        cv2.waitKey()
