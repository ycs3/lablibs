# -*- coding: utf-8 -*-

#
# win.py
# pygame, window UI related (with GL support)
#

import pygame
import math

from OpenGL.GL import *
from OpenGL.GLU import *
from OpenGL.GLUT import *
from pygame.locals import *


def is_sequence(arg):
    return (not hasattr(arg, "strip") \
            and hasattr(arg, "__getitem__") \
            or hasattr(arg, "__iter__"))

# utility classes: Win2D, Win3D, Input

#
# Win2D
#
# 2D screen using pygame
#
# drawing: text, line, rect, circle
# processing: event, clear, update
#

class Win2D:
    def __init__(self, title=None, res=(640, 480), \
            fullscreen=False, default_font=None):
        # Initialize Pygame, set resolution
        pygame.init()
        if title != None:
            pygame.display.set_caption(title)
        if fullscreen == False:
            self.s = pygame.display.set_mode(res, 0, 0)
        else:
            self.s = pygame.display.set_mode(res, pygame.FULLSCREEN, 0)
        self.win_x = res[0]
        self.win_y = res[1]

        # Initialize default font and font structure
        # font data structure: self.fonts[name][size]
        self.default_font = default_font
        self.fonts = {}

    def _get_font_match(self, name, size):
        font = None

        # try name as filename
        try:
            font = pygame.font.Font(name, size)
            return font
        except IOError:
            pass

        # try name as font on system
        fontpath = pygame.font.match_font(name)
        if is_sequence(fontpath):
            fontpath = fontpath[0]
        if fontpath != None:
            try:
                font = pygame.font.Font(fontpath, size)
                return font
            except IOError:
                pass

        # look at all system fonts, see if there is any match
        fontlist = pygame.font.get_fonts()
        matching_fonts = []
        for item in fontlist:
            if name in item:
                matching_fonts.append(item)
        if len(matching_fonts) > 0:
            sorted(matching_fonts, key=len)
            fontpath = pygame.font.match_font(matching_fonts[0])
            if fontpath != None:
                try:
                    font = pygame.font.Font(fontpath, size)
                    return font
                except IOError:
                    pass

        # We don't know, use pygame internal font
        fontpath = pygame.font.get_default_font()
        font = pygame.font.SysFont(fontpath, size)
        return font

    def _get_font(self, size, name=None):
        # default values
        if size == None:
            size = 16

        # See if fontset exists, otherwise create structure
        try:
            fontset = self.fonts[name]
        except KeyError:
            self.fonts[name] = {}

        # See if font/size exists, otherwise create new font object
        try:
            font = self.fonts[name][size]
        except KeyError:
            self.fonts[name][size] = self._get_font_match(name, size)
            font = self.fonts[name][size]
        return font

    def _textout(self, origin, text, fntsz=None, h="l", v="t", color=(0, 0, 0), font=None):
        # Single line only text output
        x, y = origin
        fontobj = self._get_font(fntsz, font)
        ren = fontobj.render(str(text), 1, color)
        wt, ht = ren.get_width(), ren.get_height()
        if h in ["c", "center"]:
            x -= int(wt*.5)
        elif h in ["r", "right"]:
            x -= wt
        if v in ["m", "middle"]:
            y -= int(ht*.4)
        elif v in ["b", "bottom"]:
            y -= ht
        self.s.blit(ren, (x, y))

    def _getlinewidth(self, width, text, fntsz=None, font=None):
        if width == None:
            return text, ""
        fontobj = self._get_font(fntsz, font)
        words = text.split()
        num_words = len(words)
        line = " ".join(words[:num_words])
        w = self._textwidth(line, fntsz, font)
        while w > width:
            if num_words == 1:
                break
            num_words -= 1
            line = " ".join(words[:num_words])
            w = self._textwidth(line, fntsz, font)
        return " ".join(words[:num_words]), " ".join(words[num_words:])

    def _textwidth(self, text, fntsz=16, font=None):
        fontobj = self._get_font(fntsz, font)
        wt, ht = fontobj.size(text)
        return wt

    def _textheight(self, fntsz=16, font=None):
        fontobj = self._get_font(fntsz, font)
        return fontobj.get_linesize()

    def text(self, origin, text, fntsz=None, color=(0, 0, 0), \
            hori="left", vert="top", font=None, width=None, line_height=1.5):
        # Multi-line text output
        # Draw text at origin (x, y) with maximum width of bounding box set to width
        # Origin is at left/center/right-top/middle/bottom corner
        fontobj = self._get_font(fntsz, font)
        text_arr = []
        rest = text
        while True:
            line_text, rest = self._getlinewidth(width, rest, fntsz, font)
            text_arr.append(line_text)
            if len(rest) == 0:
                break
        x, y = origin
        line_spacing = (fontobj.get_ascent() + fontobj.get_descent()) * line_height
        for line in text_arr:
            self._textout((x, y), line, fntsz, hori, vert, color, font)
            y += line_spacing

    def line(self, origin, dest=None, thickness=None, color=(0, 0, 0), \
            vert=None, hori=None):
        # draw point at origin
        # use dest=(x, y) to draw arbitrary line from origin to (x, y)
        # use vert=y to draw vertical line from origin of length y
        # use hori=x to draw horizontal line from origin of length x
        x1, y1 = origin
        if thickness == None:
            thickness = 1
        if dest != None:
            x2, y2 = dest
            pygame.draw.line(self.s, color, (x1, y1), (x2, y2), thickness)
        elif vert != None:
            pygame.draw.line(self.s, color, (x1, y1), (x1, y1+vert), thickness)
        elif hori != None:
            pygame.draw.line(self.s, color, (x1, y1), (x1+hori, y1), thickness)
        else:
            pygame.draw.line(self.s, color, (x1, y1), (x1, y1), thickness)

    def rect(self, origin, size, border=None, color=(0, 0, 0)):
        x, y = origin
        if border == None:
            border = 1
        wt, ht = size
        pygame.draw.rect(self.s, color, (x, y, wt, ht), border)

    def circle(self, origin, rad, border=None, color=(0, 0, 0)):
        x, y = origin
        if border == None:
            border = 1
        pygame.draw.circle(self.s, color, (int(x), int(y)), rad, border)

    # draws a numpy array (cv2 RGB image) to window
    def image(self, image, origin=None):
        if origin == None:
            ox, oy = (0, 0)
        else:
            ox, oy = origin
        h, w = image.shape[0:2]
        im = pygame.image.fromstring(image.tostring(), (w, h), "RGB")
        self.s.blit(im, (ox, oy))

    # functions for screen and input processing
    # [Example]
    # while True:
    #   Win2D.event()
    #   Win2D.clear()
    #   [.. draw here..]
    #   Win2D.update()

    def event(self):
        # Process just the exiting events for programs that do not use Input
        # The ESC key and the EXIT button; other keys are processed by Input
        # Also empties the event queue
        for event in pygame.event.get():
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_ESCAPE:
                    exit()
            if event.type == pygame.QUIT:
                exit()

    def clear(self, color=(255, 255, 255)):
        self.s.fill(color)

    def update(self):
        pygame.display.flip()

#
# Win3D
#
# OpenGL screen using PyGame
#

class Win3D:
    def __init__(self, title=None, res=(640, 480),\
            fullscreen=False, default_font=None):
        # more info on transforms: http://www.songho.ca/opengl/gl_transform.html
        # Initialize Pygame, set resolution
        pygame.init()
        if title != None:
            pygame.display.set_caption(title)
        if fullscreen == False:
            self.s = pygame.display.set_mode(res, OPENGL|DOUBLEBUF, 0)
        else:
            self.s = pygame.display.set_mode(res, OPENGL|DOUBLEBUF|FULLSCREEN, 0)
        self.win_x = res[0]
        self.win_y = res[1]

        # Initialize default font and font structure
        # font data structure: self.fonts[name][size]
        self.default_font = default_font
        self.fonts = {}

        # OpenGL stuff starts
        glutInit()
        glClearColor(0., 0., 0., 0.) # sets the color for glClear
        glShadeModel(GL_FLAT) # No shading
        self.refresh()

        # (imagine your head when ->)
        # camera view matrix in degrees
        self.pitch_x = 0.   # (+) looking up   (-) looking down
        self.heading_y = 180. # (+) looking left (-) looking right
        self.roll_z = 0.    # (+) leaning left (-) leaning right

        # camera placement
        self.pos_x = 0.     # (+) step right   (-) step left
        self.pos_y = 0.     # (+) step up      (-) step down
        self.pos_z = -2.     # (+) step back    (-) step forward
        # let's say by default the center of the model is 2m away from the camera

        # (Assume someone is facing you and their face ->)
        # placement of model
        self.off_x = 0.  # (+) moves to their left (-) moves to their right
        self.off_y = 0.  # (+) moves up            (-) moves down
        self.off_z = 0.  # (+) moves towards you   (-) moves away from you

        # rotation of model
        self.rot_x = 0.  # (+) moves down           (-) moves up
        self.rot_y = 0.  # (+) moves to their left  (-) moves to their right
        self.rot_z = 0.  # (+) leans to their right (-) leans to their left

        # temporary values to keep track of mouse dragging
        self.temp_xy = [(0., 0.), (0., 0.), (0., 0.)]

    def refresh(self):
        glViewport(0, 0, self.win_x, self.win_y)
        glMatrixMode(GL_PROJECTION)
        glLoadIdentity() # Initialize PROJECTION matrix
        fov_h, fov_v = 57.8, 43.0 # fov in degrees for the kinect
        # kinect depth sensor range of 1.2m - 3.5m
        # assuming 1 unit in GL is 1m
        gluPerspective(fov_v, fov_h/fov_v, .1, 15.)
        glMatrixMode(GL_MODELVIEW)

    def draw_debugaxis(self, size=1., draw_cube=True):
        # draw wirecube at with sides of size
        if draw_cube == True:
            glColor3f(1., 1., 1.)
            glutWireCube(size)

        # draw lines extending out the x, y and z axes
        glColor3f(1., 0., 0.)
        glBegin(GL_LINES)
        glVertex3f(0., 0., 0.)
        glVertex3f(size, 0., 0.) # x axis
        glEnd()
        glColor3f(0., 1., 0.)
        glBegin(GL_LINES)
        glVertex3f(0., 0., 0.)
        glVertex3f(0., size, 0.) # y axis
        glEnd()
        glColor3f(0., 0., 1.)
        glBegin(GL_LINES)
        glVertex3f(0., 0., 0.)
        glVertex3f(0., 0., size) # z axis
        glEnd()

        glColor3f(1., 1., 1.)
        glFlush()

    def draw_prep(self, debug=False):
        glClear(GL_COLOR_BUFFER_BIT)
        glColor3f(1., 1., 1.)
        glLoadIdentity() # Initialize MODELVIEW matrix

        # MODELVIEW matrix
        glRotatef(-self.roll_z, 0, 0, 1)
        glRotatef(-self.heading_y, 0, 1, 0)
        glRotatef(-self.pitch_x, 1, 0, 0)
        glTranslatef(-self.pos_x, -self.pos_y, -self.pos_z)

        # Everything drawn now is relative to the origin

        # we now translate the model, drawing at x, y, z with rotation
        glTranslatef(self.off_x, self.off_y, self.off_z)
        glRotatef(self.rot_x, 1, 0, 0)
        glRotatef(self.rot_y, 0, 1, 0)
        glRotatef(self.rot_z, 0, 0, 1)

        if debug == True:
            self.draw_debugaxis(.5, True)

    # functions for screen and input processing
    # [Example]
    # while True:
    #   Win3D.event()
    #   Win3D.clear()
    #   [.. draw here..]
    #   Win3D.update()

    def event(self, act=[], control=False):
        # we use Input to track mouse dragging
        for e in act:
            if e.startswith("m_move0_"):
                x1, y1, x2, y2 = map(float, e.split("_")[2:6])
                if self.temp_xy[0] == (0., 0.):
                    # new drag sequence, record original point
                    self.temp_xy[0] = (x2, y2)
                dx, dy = x1 - self.temp_xy[0][0], y1 - self.temp_xy[0][1]
                self.rot_x -= (dy * .2)
                self.rot_y += (dx * .2)
                self.temp_xy[0] = (x1, y1)
                break
        for e in act:
            if e.startswith("m_move2_"):
                x1, y1, x2, y2 = map(int, e.split("_")[2:6])
                if self.temp_xy[2] == (0., 0.):
                    # new drag sequence, record original point
                    self.temp_xy[2] = (x2, y2)
                dx, dy = x1 - self.temp_xy[2][0], y1 - self.temp_xy[2][1]
                self.pos_z -= (dx * .01)
                self.temp_xy[2] = (x1, y1)
                break
        for e in act:
            for i in range(len(self.temp_xy)):
                if e.startswith("m_drag" + str(i) + "_"):
                    # drag sequence finished, remove points
                    self.temp_xy[i] = (0., 0.)
                    #break

        # implement simple input to control rotation of model
        if control == True:
            for e in act:
                if e.startswith("a_q"):
                    self.rot_x += 1
                elif e.startswith("a_w"):
                    self.rot_x -= 1
                elif e.startswith("a_a"):
                    self.rot_y += 1
                elif e.startswith("a_s"):
                    self.rot_y -= 1
                elif e.startswith("a_z"):
                    self.rot_z += 1
                elif e.startswith("a_x"):
                    self.rot_z -= 1
                elif e.startswith("k_up"):
                    self.off_z += .1
                elif e.startswith("k_down"):
                    self.off_z -= .1

        # Process just the exiting events for programs that do not use Input
        # The ESC key and the EXIT button; other keys are processed by Input
        # Also empties the event queue
        for event in pygame.event.get():
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_ESCAPE:
                    exit()
            if event.type == pygame.QUIT:
                exit()

    def clear(self, debug=False):
        self.draw_prep(debug)

    def update(self, debug=False):
        if debug == True:
            self.draw_debugaxis(.5, True)
        pygame.display.flip()

#
# Input
#
# Note: The event queue is left as-is (queue needs to be emptied separately)
# pygame.event.get() empties the queue
#

class Input:
    def __init__(self, joystick_num=0, debug=False, use_joystick=False):
        if pygame.joystick.get_init() == False:
            if debug == True:
                print "Initializing joystick module..."
            pygame.joystick.init()
        self.joystick_count = pygame.joystick.get_count()
        self.have_joystick = False
        if debug == True:
            print self.joystick_count, "joystick(s) detected."
        pygame.key.set_repeat(100, 50)
        if self.joystick_count >= 1 and use_joystick == True:
            self.have_joystick = True
            print "Initializing joystick number", joystick_num
            self.joystick = pygame.joystick.Joystick(joystick_num)
            self.joystick.init()
            self.name = self.joystick.get_name()
            self.axes_n = self.joystick.get_numaxes()
            self.buttons_n = self.joystick.get_numbuttons()
            self.hats_n = self.joystick.get_numhats()
            print self.name
            print "    [axes]", self.axes_n
            print " [buttons]", self.buttons_n
            print "    [hats]", self.hats_n
            self.direction_threshold = 2
            self.axes_ready = True

            self.cur_dir = None
            self.cur_dir_idx = 0
            self.cur_dir_threshold = 12

        self.cur_mouse_drag = None
        self.cur_right_drag = None
        self.cur_drag = [None, None, None]

    def _get_stick(self, start_axis=0):
        x = self.joystick.get_axis(start_axis)
        y = self.joystick.get_axis(start_axis+1)
        deg = math.degrees(math.atan2(y, x)) + 90
        if deg < 0:
            deg = 360 + deg
        deg = int(deg)
        dist = int(math.sqrt(x*x+y*y)*10)
        if dist == 0:
            deg = 0
        return dist, deg

    def _get_throttle(self, start_axis=2):
        x = self.joystick.get_axis(start_axis)
        return int(((1-x)/2)*10)

    def event(self):
        ret = self.get_buttons()
        ret.extend(self.get_direction())
        ret.extend(self.get_keys())
        ret.extend(self.get_mouse())
        return ret

    def get_keys(self):
        ret = set()
        pygame.event.pump()
        actions = [pygame.KEYDOWN]
        if pygame.event.peek(actions) == True:
            for evt in pygame.event.get(actions):
                if evt.key == pygame.K_LEFT:
                    ret.add("k_left")
                elif evt.key == pygame.K_RIGHT:
                    ret.add("k_right")
                elif evt.key == pygame.K_UP:
                    ret.add("k_up")
                elif evt.key == pygame.K_DOWN:
                    ret.add("k_down")
                elif evt.key == pygame.K_BACKSPACE:
                    ret.add("a_backspace")
                elif evt.key == pygame.K_SPACE:
                    ret.add("a_space")
                elif evt.key >= pygame.K_a and evt.key <= pygame.K_z:
                    ret.add("a_" + chr(evt.key))
                elif evt.key >= pygame.K_0 and evt.key <= pygame.K_9:
                    ret.add("a_" + chr(evt.key))
                pygame.event.post(evt)
        return list(ret)

    def get_mouse(self):
        ret = []
        pygame.event.pump()
        actions = [pygame.MOUSEMOTION]
        if pygame.event.peek(actions) == True:
            for i in range(len(self.cur_drag)):
                if self.cur_drag[i] == None:
                    if pygame.mouse.get_pressed()[i] == 1:
                        self.cur_drag[i] = (-1, -1)
            for evt in pygame.event.get(actions):
                for i in range(len(self.cur_drag)):
                    if self.cur_drag[i] != None:
                        x, y = evt.pos
                        if self.cur_drag[i] == (-1, -1):
                            self.cur_drag[i] = (x, y)
                        ox, oy = self.cur_drag[i]
                        if abs(x-ox) >= 3 or abs(y-oy) >= 3:
                            ret.append("m_move" + str(i) + "_" + str(x) +\
                                    "_" +str(y) + "_" + str(ox) + "_" + str(oy))
                pygame.event.post(evt)
        actions = [pygame.MOUSEBUTTONUP]
        if pygame.event.peek(actions) == True:
            for evt in pygame.event.get(actions):
                button = evt.button - 1
                for i in range(len(self.cur_drag)):
                    if button == i and self.cur_drag[i] != None:
                        x, y = evt.pos
                        ox, oy = self.cur_drag[i]
                        if abs(x-ox) < 3 and abs(y-oy) < 3:
                            ret.append("m_click" + str(i) + "_" + str(x) +\
                                    "_" + str(y))
                        else:
                            ret.append("m_drag" + str(i) + "_" + str(x) +\
                                    "_" + str(y) + "_" + str(ox) + "_" + str(oy))
                        self.cur_drag[i] = None
                    elif button == i:
                        x, y = evt.pos
                        ret.append("m_click" + str(i) + "_" + str(x) + "_" + str(y))
                pygame.event.post(evt)
        return ret

    def get_buttons(self):
        ret = set()
        pygame.event.pump()
        actions = [pygame.JOYBUTTONDOWN]
        if pygame.event.peek(actions) == True and self.have_joystick == True:
            for i in range(self.buttons_n):
                if self.joystick.get_button(i) == 1:
                    ret.add("b_" + str(i+1))
        return list(ret)

    def _deg_to_area(self, deg):
        return int(deg/11.25)

    def get_direction(self):
        ret = set()
        pygame.event.pump()
        actions = [pygame.JOYAXISMOTION]
        if pygame.event.peek(actions) == True and self.have_joystick == True:
            if self.axes_ready == True:
                for i in range(0, self.axes_n-1, 2):
                    idx = "j" + str(int(i/2))
                    dist, deg = self._get_stick(i)
                    if dist >= self.direction_threshold:
                        area = self._deg_to_area(deg)
                        if area == 31 or area == 0:
                            self.cur_dir = idx + "_up"
                        elif area == 1 or area == 2 or area == 3:
                            self.cur_dir = idx + "_up_right"
                        elif area == 4 or area == 5 or area == 6:
                            self.cur_dir = idx + "_right_up"
                        elif area == 7 or area == 8:
                            self.cur_dir = idx + "_right"
                        elif area == 9 or area == 10 or area == 11:
                            self.cur_dir = idx + "_right_down"
                        elif area == 12 or area == 13 or area == 14:
                            self.cur_dir = idx + "_down_right"
                        elif area == 15 or area == 16:
                            self.cur_dir = idx + "_down"
                        elif area == 17 or area == 18 or area == 19:
                            self.cur_dir = idx + "_down_left"
                        elif area == 20 or area == 21 or area == 22:
                            self.cur_dir = idx + "_left_down"
                        elif area == 23 or area == 24:
                            self.cur_dir = idx + "_left"
                        elif area == 25 or area == 26 or area == 27:
                            self.cur_dir = idx + "_left_up"
                        elif area == 28 or area == 29 or area == 30:
                            self.cur_dir = idx + "_up_left"
                        ret.add(self.cur_dir)
            self.axes_ready = True
            for i in range(0, self.axes_n-1, 2):
                dist, deg = self._get_stick(i)
                if dist >= self.direction_threshold:
                    self.axes_ready = False
            if self.axes_ready == True:
                self.cur_dir = None

        if self.have_joystick == True:
            if self.cur_dir != None:
                self.cur_dir_idx += 1
                if self.cur_dir_idx == self.cur_dir_threshold:
                    ret.add("l" + self.cur_dir[1:])
                elif self.cur_dir_idx % self.cur_dir_threshold == 0:
                    ret.add("n" + self.cur_dir[1:])
            else:
                self.cur_dir_idx = 0
        return list(ret)


#
# Examples
#

def win3d_example():
    s = Win3D()
    j = Input()
    while True:
        act = j.event()
        s.event(act)
        if len(act) > 0:
            print act
        s.clear(True)
        s.update()

def win2d_example():
    import cv2
    im = cv2.imread("test.jpg")
    im = cv2.cvtColor(im, cv2.COLOR_BGR2RGB)
    s = Win2D()
    j = Input()
    while True:
        act = j.event()
        s.event()
        if len(act) > 0:
            print act
        s.clear()
        s.image(im, (10, 10))
        s.text((100, 100), "hello World", 20, width=50, font="malgun")
        s.line((100, 100), hori=50, thickness=5)
        s.update()

if __name__ == '__main__':
    win3d_example()
